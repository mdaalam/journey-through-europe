package Handlers;

import static Application.Variables.*;
import JourneyUI.JTEUI;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Line;

public class PlayerMousePressedHandler implements EventHandler<MouseEvent> {
    public JTEUI ui;
    public PlayerMousePressedHandler(JTEUI ui) {
        this.ui = ui;
        timesPieceClicked = 0;
    }

    @Override
    public void handle(MouseEvent event) {   
        if(timesPieceClicked == 0){
            for (Line l : ui.linesToNeighbors) {
                ui.center.getChildren().add(l);
            }
            timesPieceClicked ++;
        }else if(timesPieceClicked == 1){
            for (Line l : ui.linesToNeighbors) {
                ui.center.getChildren().remove(l);
            }
            timesPieceClicked --;
        }
    }
}
