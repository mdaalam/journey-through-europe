package Handlers;

import Application.Main;
import static Application.Variables.*;
import static Application.Variables.timesPieceClicked;
import JourneyUI.JTEUI;
import Objects.City;
import Objects.Player;
import Objects.Point;
import java.util.ArrayList;
import javafx.animation.Animation;
import javafx.animation.Transition;
import javafx.animation.TranslateTransition;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.util.Duration;

public class MapCitiesHandler implements EventHandler<MouseEvent> {
    public JTEUI ui;
    public City selectedCity = null;
    public static Circle circle = new Circle(15);
    public static boolean circlePresent;

    public MapCitiesHandler(JTEUI ui) {
        this.ui = ui;
        selectedCity = null;
        circlePresent = false;
    }

    @Override
    public void handle(MouseEvent event) {
        clickToMap(event.getX(), event.getY());
    }

    public void clickToMap(double x, double y) {
        clickToMap(x, y, false);
    }

    public void clickToMap(double x, double y, boolean comp) {
        Point p = new Point((int) x, (int) y);
        Player player = ui.players.get(ui.getCurrentPlayer());
        City currentcity = player.currentCity;
        selectedCity = ui.getCityAt(p);

        if (!player.hasRolled() && player.getNumMoves() <= 0) {
            if (selectedCity != null) {
                ui.loadFailureToRollDialogBox();
            }
            return;
        }
        if (selectedCity == null) {
            System.out.println("selected city is null");
            ui.center.getChildren().remove(circle);
            circlePresent = false;
            return;
        } else {
            System.out.println("printing neighbors of: " + selectedCity.name);
            for (int i = 0; i < selectedCity.landNeighbors.size(); i++) {
                debugPrint(mapClickNeighbors, "\tland: " + selectedCity.landNeighbors.get(i).name);
            }
            for (int i = 0; i < selectedCity.seaNeighbors.size(); i++) {
                debugPrint(mapClickNeighbors, "\tsea: " + selectedCity.seaNeighbors.get(i).name);
            }
            debugPrintln(mapClickNeighbors, "");
        }
        debugPrintln(mapClickTrace, "checkpoint 4");
        if (player.previousCity != null) {
            debugPrintln(mapClickTrace, "checkpoint 4a");
            if (player.previousCity == selectedCity) {
                debugPrintln(mapClickTrace, "checkpoint 4b");
                ui.center.getChildren().remove(circle);
                circlePresent = false;
                return;
            }
        }
        debugPrintln(mapClickTrace, "checkpont 5");
        boolean isLand = false;
        boolean isSea = false;
        boolean isAir = false;

        ArrayList<City> land = player.landCities;
        ArrayList<City> sea = player.seaCities;
        ArrayList<City> air = player.airCities;

        for (City c : land) {
            if (isLand) {
                break;
            }
            isLand |= (c.x == selectedCity.x && c.y == selectedCity.y && c.quadrent == selectedCity.quadrent);
        }

        for (City c : sea) {
            if (isSea) {
                break;
            }
            isSea |= (c.x == selectedCity.x && c.y == selectedCity.y && c.quadrent == selectedCity.quadrent);
        }
        
        for (City c : air) {
            if (isAir) {
                break;
            }
            isAir |= (c.x == selectedCity.x && c.y == selectedCity.y && c.quadrent == selectedCity.quadrent);
        }
        
        if(isAir && ! isSea && ! isLand){
            System.out.println("taking air route");
            ui.flightPlanClickLoopHole(selectedCity.airPointCoords.x, selectedCity.airPointCoords.y);
            clickToMap(0, 0);
        }
        else if (isSea || isLand) {

            if (circlePresent || player.seaRouteCity != null) {
                
                if(isSea && player.seaRouteCity == null){
                    player.turnComplete();
                    player.seaRouteCity = selectedCity;
                    
                }else{
                    if(player.seaRouteCity != null){
                        player.seaRouteCity = null;
                    }
                debugPrintln(mapClickTrace, "checkpoint 7");
                Animation animation = new Transition() {
                {
                    setCycleDuration(Duration.millis(1000));//Duration.millis((comp) ? 2000 : 1000));
                }

                    protected void interpolate(double frac) {
                    }
                };

                TranslateTransition tt = null;
                if (false && comp) {
                    debugPrint(ComputerTesting, "COMPUTER - setting delays for animation and transition");
                    tt = new TranslateTransition(new Duration(2000), player.ivGame);
                    tt.setDelay(Duration.millis(1000));
                    
                    animation.setDelay(Duration.millis(1000));
                } else {
                    
                    debugPrint(ComputerTesting, "HUMAN - creating delay for human");
                    
                    
                    tt = new TranslateTransition(new Duration(Main.CardLoadSpeed/2), player.ivGame);
                }

                City normailzation = selectedCity.validateAndUpdateCity(currentcity);
                player.setPlayerAndImageX(normailzation.xReal - player.ivGame.getImage().getWidth()/2);
                player.setPlayerAndImageY(normailzation.yReal - player.ivGame.getImage().getHeight());
                    System.out.println("please dont fucking die on me");
                Point target = new Point(selectedCity.xReal - normailzation.xReal, selectedCity.yReal - normailzation.yReal);
                player.ivGame.setDisable(true);

                ui.center.getChildren().remove(player.ivGame);
                ui.center.getChildren().add(player.ivGame);

                debugPrintln(mapClickTrace, "\t mapCitiesHandler - movePlayerToCityCalled");
                //--------------------------------------------------------------------------

                ui.movePlayerToCity(player, selectedCity, target, tt, animation, false);
                selectedCity = null;
                ui.center.getChildren().remove(circle);
                circlePresent = false;
                }
                for (Line l : ui.linesToNeighbors) {
                    ui.center.getChildren().remove(l);
                    timesPieceClicked = 0;
                }
                debugPrintln(mapClickTrace, "checkpoint 7-");
                

            } else if (ui.processMapClick(p)) {
                debugPrintln(mapClickTrace, "checkpoint 8");
                debugPrintln(mapClickTrace, "should be drawing circle around " + selectedCity.name);
                circle.setStroke(Color.GREENYELLOW);
                circle.setStrokeWidth(3);
                circle.setFill(Color.TRANSPARENT);
                circle.setCenterX(selectedCity.xReal);
                circle.setCenterY(selectedCity.yReal);
                ui.center.getChildren().add(circle);
                circlePresent = true;
            } else {
                debugPrintln(mapClickTrace, "checkpoint 9");
                ui.center.getChildren().remove(circle);
                circlePresent = false;
                debugPrintln(mapClickTrace, "click for map here");
            }
        } else {
            debugPrintln(mapClickTrace, "checkpoing 10");
            debugPrintln(mapClickTrace, "city is not null but " + selectedCity.name + " is not in the neighbors");

        }
        debugPrintln(mapClickTrace, "checkpoing fuck you");
    }

    public void removeCircle() {
        ui.center.getChildren().remove(circle);
        circlePresent = false;
    }

    public static void print(String x) {
    }

    public static void println(String x) {
    }

    public static void debugPrintln(boolean x, String s) {
        if (x) {
            System.out.println(s);
        }
    }

    public static void debugPrint(boolean x, String s) {
        if (x) {
            System.out.print(s);
        }
    }
}