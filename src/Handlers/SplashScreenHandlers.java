package Handlers;

import JourneyUI.JTEUI;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class SplashScreenHandlers implements EventHandler<MouseEvent>{
    protected JTEUI ui;
    protected String name;
    public SplashScreenHandlers(JTEUI ui, String name){
        this.ui = ui;
        this.name = name;
    }
    
    @Override
    public void handle(MouseEvent event) {
            ui.changeWorkspace(name);
    }
}
