package Handlers;

import JourneyUI.JTEUI;
import static JourneyUI.JTEUI.currentMapSection;
import static JourneyUI.JTEUI.wasMoved;
import Objects.Player;
import javafx.event.EventHandler;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import static Application.Variables.*;

public class PlayerDragHandler implements EventHandler<MouseEvent>{

    public JTEUI ui;
    public PlayerDragHandler(JTEUI ui){
        this.ui = ui;
    }
    
    @Override
    public void handle(MouseEvent event) {
        
        if(! ui.players.get(ui.getCurrentPlayer()).hasRolled() && ui.players.get(ui.getCurrentPlayer()).getNumMoves() <= 0){
            ui.loadFailureToRollDialogBox();
            return;
        }
        ui.mch.removeCircle();
        Player player =ui.players.get(ui.getCurrentPlayer());
        ImageView iv = player.ivGame;
        iv.setX(event.getX() - (player.playerImageWidth/2));
        iv.setY(event.getY());
        checkBoundsForMapChange(iv, player, event);
        wasMoved = 0;
    }

    public void checkBoundsForMapChange(ImageView iv, Player player, MouseEvent event){
        if(iv.getX() < 0){
            iv.setX(0);
        }
        if(iv.getX() > ui.paneWidth/2 - player.playerImageWidth){
            iv.setX((ui.paneWidth) - player.playerImageWidth);
        }
        if(iv.getY() < 0){
            iv.setY(0);
        }
        if(iv.getY() > ui.paneHeight - 20){
            iv.setY(ui.paneHeight - 20);
        }
        
        if(currentMapSection == 1){
            if(event.getX() > (ui.paneWidth/2) - borderArea){
                ui.changeWorkspace("top right");
            }
            if(event.getY() > (ui.paneHeight) - borderArea){
                ui.changeWorkspace("bottom left");
            }
        }else if(currentMapSection == 2){
            if(event.getX() < borderArea){
                ui.changeWorkspace("top left");
            }
            if(event.getY() > (ui.paneHeight) - borderArea){
                ui.changeWorkspace("bottom right");
            }
        }else if(currentMapSection == 3){
            if(event.getX() > (ui.paneWidth/2) - borderArea){
                ui.changeWorkspace("bottom right");
            }
            if(event.getY() < borderArea){
                ui.changeWorkspace("bottom left");
            }
        }else if(currentMapSection == 4){
            if(event.getX() < borderArea){
                ui.changeWorkspace("top right");
            }
            if(event.getY() < borderArea){
                ui.changeWorkspace("bottom left");
            }
        }
    }
}