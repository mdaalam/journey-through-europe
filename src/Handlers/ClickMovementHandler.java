package Handlers;

import JourneyUI.JTEUI;
import Objects.City;
import Objects.Player;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.util.Duration;

public class ClickMovementHandler implements EventHandler<ActionEvent>{
    public Player player;
    public City selectedCity;
    public JTEUI ui;
    public boolean isDrag;
    
    public static int tracker = 0;
    /**
     * on finish handler for the tralnslate transisition for the player peices
     * @param player
     * @param exactSelectedCity
     * @param ui
     * @param isDrag 
     */
    public ClickMovementHandler(Player player, City exactSelectedCity, JTEUI ui, boolean isDrag){
        this.player = player;
        this.selectedCity = exactSelectedCity;
        this.ui = ui;
        this.isDrag = isDrag;
    }
    @Override
    public void handle(ActionEvent event) {     
        if(! player.hasRolled() && player.getNumMoves() <= 0){
            return;
        } 
        player.ivGame.setTranslateX(0);
        player.ivGame.setTranslateY(0);
            player.setPlayerAndImageX(selectedCity.xReal - player.playerImageWidth/2);
            player.setPlayerAndImageY(selectedCity.yReal - player.playerImageHeight); 
        System.out.println(tracker + "-" +tracker + " - currCity: " + player.currentCity.name + " --> selected: " + selectedCity.name);
        if(player.moveCurrentCity(selectedCity)){
            return;
        }
        
        System.out.println(tracker + "-" +tracker + " - currCity: " + player.currentCity.name + " --> selected: " + selectedCity.name); 
        if(player.getNumMoves() <= 0 && player.hasRolled()){
            if(false){
                TranslateTransition tt = new TranslateTransition(Duration.millis(500), new Button());
                tt.setToX(500);
                System.out.println("\t+starting time lkiller in ClickMovementHandler");
                tt.play();
                tt.setOnFinished(new EventHandler<ActionEvent>(){
                    @Override
                    public void handle(ActionEvent event) {
                        System.out.println("\t+in onfinishhandler for time killer");
                        player.turnComplete();
                    }
                });
            }else{
                player.turnComplete();
            }
            
        }else if(player.getNumMoves() > 0 && player.isComputer()){
            System.out.println("\tcalling autoRun from ClickMovementHandler");
            ui.autoRun(player.getNumMoves());
            
        }
        ui.initLinesToNeighbors();
        player.ivGame.setDisable(false);
    }
}
