package Handlers;

import JourneyUI.JTEUI;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class DieHandler implements EventHandler<MouseEvent>{
    private JTEUI ui;
    public DieHandler(JTEUI ui){
        this.ui = ui;
    }
    @Override
    public void handle(MouseEvent event) {
        loopHole();
    }
    
    public void loopHole(){
        System.out.println("processing die click");
        int num = (int)(Math.random()*6)+1;
        ui.players.get(ui.getCurrentPlayer()).history.add("Rolled: " + num);
        if( ! ui.players.get(ui.getCurrentPlayer()).hasRolled()){
            System.out.println("player die click will be processed");
            ui.processDiceClick(num);
            
        }
        System.out.println("ui.updateNumMovesText(): " + num);
    }
}
