package Handlers;

import Application.Main;
import static Application.Variables.ComputerTesting;
import static Application.Variables.timesPieceClicked;
import static Handlers.MapCitiesHandler.debugPrint;
import JourneyUI.JTEUI;
import static JourneyUI.JTEUI.wasMoved;
import Objects.City;
import Objects.Player;
import Objects.Point;
import java.util.ArrayList;
import javafx.animation.Animation;
import javafx.animation.Transition;
import javafx.animation.TranslateTransition;
import javafx.event.EventHandler;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Line;
import javafx.util.Duration;

public class PlayerMouseReleaseHandler implements EventHandler<MouseEvent> {
    public JTEUI ui;
    public PlayerMouseReleaseHandler(JTEUI ui) {
        this.ui = ui;
    }

    @Override
    public void handle(MouseEvent event) {
        
        if (wasMoved != -1) {
            Point p = new Point((int)event.getX(), (int)event.getY());
            City selectedCity = ui.getCityAt(p);
            Player player = ui.players.get(ui.getCurrentPlayer());
            City currentcity = player.currentCity;
            ImageView iv = ui.players.get(ui.getCurrentPlayer()).ivGame;
            for(int i = 0; i < iv.getImage().getHeight(); i += 2){
                if(selectedCity == null){
                    selectedCity = ui.getCityAt(new Point((int)event.getX(), (int)event.getY() + i)); 
                }else{
                    break;
                }
            }
            if (!player.hasRolled() && player.getNumMoves() <= 0) {
                if (selectedCity != null) {
                    ui.loadFailureToRollDialogBox();
                }
                return;
            }
            if(selectedCity == null || selectedCity == currentcity){
                System.out.println("attempt to move to non-neighbor caught");
                ui.changeWorkspace(ui.players.get(ui.getCurrentPlayer()).currentCity.quadrent);
                player.ivGame.setX(ui.players.get(ui.getCurrentPlayer()).x);
                player.ivGame.setY(ui.players.get(ui.getCurrentPlayer()).y);
                clearLines();
                wasMoved = 0;
                return;
            }
            
            if(player.previousCity != null){
                if(selectedCity == player.previousCity){
                    System.out.println("attempt to move to previous node via drag caught");
                    ui.changeWorkspace(ui.players.get(ui.getCurrentPlayer()).currentCity.quadrent);
                    player.ivGame.setX(ui.players.get(ui.getCurrentPlayer()).x);
                    player.ivGame.setY(ui.players.get(ui.getCurrentPlayer()).y);
                    clearLines();
                    wasMoved = 0;
                    return;
                }
            }
            
            boolean isLand = false;
            boolean isSea = false;
            boolean isAir = false;
            ArrayList<City> land = player.landCities;
            ArrayList<City> sea = player.seaCities;
            ArrayList<City> air = player.airCities;

            for (City c : land) {
                if (isLand) {
                    break;
                }
                isLand |= (c.x == selectedCity.x && c.y == selectedCity.y && c.quadrent == selectedCity.quadrent);
            }

            for (City c : sea) {
                if (isSea) {
                    break;
                }
                isSea |= (c.x == selectedCity.x && c.y == selectedCity.y && c.quadrent == selectedCity.quadrent);
            }

            for (City c : air) {
                if (isAir) {
                    break;
                }
                isAir |= (c.x == selectedCity.x && c.y == selectedCity.y && c.quadrent == selectedCity.quadrent);
            }

             if(isAir && ! isSea && ! isLand){
                ui.loadFailureToRollDialogBox("You must use the flights map for this action");
        }
             if (isSea || isLand) {
                System.out.println("mouseReleaseHandler - you have moved to a valid location " );
                
                Animation animation = new Transition() {
                    {
                        setCycleDuration(new Duration(Main.CardLoadSpeed/2));
                    }

                    protected void interpolate(double frac) {
                    }
                };
                TranslateTransition tt = null;
                if (false ){
                    debugPrint(ComputerTesting, "COMPUTER - setting delays for animation and transition");
                    tt = new TranslateTransition(new Duration(2000), player.ivGame);
                    tt.setDelay(Duration.millis(1000));
                    
                    animation.setDelay(Duration.millis(1000));
                } else {
                    
                    debugPrint(ComputerTesting, "HUMAN - creating delay for human");
                    
                    
                    tt = new TranslateTransition(new Duration(Main.CardLoadSpeed), player.ivGame);
                }
                double deltaY = player.ivGame.getY() - selectedCity.yReal;
                double deltaX = selectedCity.xReal - event.getX();
                Point target = new Point((int)deltaX, 0 - (int)(player.ivGame.getImage().getHeight() - deltaY));
                System.out.println("\t releasehandler - movePlayerToCityCalled");
                ui.movePlayerToCity(player, selectedCity, target, tt, animation, true);

                clearLines();
                ui.initLinesToNeighbors();

                for(int i= 0; i < player.landCities.size(); i ++){
                }
            }else{
                player.ivGame.setX(ui.players.get(ui.getCurrentPlayer()).x);
                player.ivGame.setY(ui.players.get(ui.getCurrentPlayer()).y);
                clearLines();
            }
            clearLines();
        }
        wasMoved = -1;
    }
    
    public void clearLines(){
        for (Line l : ui.linesToNeighbors) {
                ui.center.getChildren().remove(l);
                timesPieceClicked = 0;
            }
    }
}