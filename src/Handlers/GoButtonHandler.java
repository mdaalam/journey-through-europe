package Handlers;

import Objects.Player;
import JourneyUI.*;
import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;

public class GoButtonHandler implements EventHandler<ActionEvent>{
    public JTEUI ui;
    private HBox b;
    private Label err;
    public GoButtonHandler(JTEUI ui){
        this.ui = ui;
        this.b = new HBox();
        err = new Label();
    }
    
    public void handle(ActionEvent event) {
        goButtonHandler();
    }
    public void goButtonHandler(){
        boolean pass = true;
        ArrayList<Player> players = JTEUI.players;
        pass &= (JTEUI.numPlayers >= 2);
        if(pass){
            ui.changeWorkspace("go");
        }else{
            if(ui.playerPaneTop.getChildren().contains(b))
                return;
            b.setPrefSize(500, 100);
            if(false && JTEUI.players.get(0).isComputer()){
                err.setText("Player 1 must be human");
            }else{
                err.setText("You must select at least 2 players");
            }
            err.setFont(Font.font ("Verdana", 25));
            err.setStyle("-fx-font-fill: black");
            b.setAlignment(Pos.TOP_RIGHT);
            b.getChildren().add(err);
            ui.playerPaneTop.getChildren().add(err);
        }
    }
}