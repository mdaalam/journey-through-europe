package Handlers;

import JourneyUI.JTEUI;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import static Application.Main.*;

public class AnimationOnFinishedHandler implements EventHandler<ActionEvent>{
    public JTEUI ui;
    public AnimationOnFinishedHandler(JTEUI ui){
        this.ui = ui;
    }
    
    public static boolean initCond = true;

    @Override
    public void handle(ActionEvent event) {
        int size = ui.players.get(ui.getCurrentPlayer()).cards.size(); 
        ui.lock.lock();
        if(size == 1){ // account for one for the label
            ui.changeToNextPlayer();
        }
        if(initCond){
            if(ui.players.get(ui.numPlayers - 1).cards.size() == numCardsAllowedInHand ){
                initCond = false;
            }
            if(size < numCardsAllowedInHand){
                ui.loadCardsForPlayers();
            }else if(size == numCardsAllowedInHand ){
                ui.changeToNextPlayer();
                if(initCond){
                    ui.loadCardsForPlayers();
                }
            }
            if(! initCond){
                ui.continueRunningGame();
            }
        }
        else if(size < numCardsAllowedInHand){ 
            ui.loadCardsForPlayers();
        }
        ui.lock.unlock(); 
    }
}