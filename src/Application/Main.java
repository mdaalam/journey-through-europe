package Application;

import JourneyUI.*;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application{
    public static double CardLoadSpeed = 1000;
    public static int numCardsAllowedInHand = 4;
    public static boolean playerCanMove = false;
    
    public static void main(String[] args) {
        Variables v = new Variables();
        GregorianCalendar calendar = new GregorianCalendar(2014, 12, 5);
        if(Calendar.getInstance().before(calendar)){
            System.out.println("Project is before submission date: fps for movement has been increased");
            CardLoadSpeed = 1000;
        }
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        try{
            primaryStage.setTitle("Journey Through Europe");
            JTEUI root = new JTEUI();
            BorderPane mainPane = root.getMainPane();
            root.setStage(primaryStage);
            Scene scene = new Scene(mainPane, mainPane.getWidth(), mainPane.getHeight());
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }   
    }
}