package Application;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Variables {
    public static int timesPieceClicked = 0;
    public static int borderArea = 2;
    public static boolean mapClickTrace = false;
    public static boolean mapClickNeighbors = false;
    
    //playerClass
    public static boolean PlayerMethodCalls;
    public static boolean PlayerTesting;
    public static boolean ComputerTesting;
    public static boolean DijkstraTesting;

    public Variables(){
        GregorianCalendar calendar = new GregorianCalendar(2014, 12, 5);
        if(Calendar.getInstance().before(calendar)){
            mapClickTrace = false;
            PlayerMethodCalls = false;
            ComputerTesting = false;
            DijkstraTesting = false;
                    
        }
    }
    
    public static void debugPrint(boolean b, String x){
        if(b){
            System.out.print(x);
        }
    }
    
    public static void debugPrintln(boolean b, String x){
        if(b){
            System.out.println(x);
        }
    }
}