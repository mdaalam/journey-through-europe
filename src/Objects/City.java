
package Objects;

import static JourneyUI.JTEDataManager.ui;
import java.util.ArrayList;

public class City implements Cloneable{
    public String name;
    public int x;
    public int y;
    public int xReal;
    public int yReal;
    public String color;
    public int quadrent;
    public ArrayList<City> landNeighbors;
    public ArrayList<City> seaNeighbors;
    public ArrayList<City> airNeighbors;
    private ArrayList<City> airportsOnFlightPlan;
    private boolean isAirport;
    public int airportQuadrent;
    public Point airPointCoords;

    public City(String name, String color, int quadrant, int x, int y, int xReal, int yReal){
        this.x = x;
        this.y = y;
        this.xReal = xReal;
        this.yReal = yReal;
        this.color = color;
        this.quadrent = quadrant;
        this.name = name;
        landNeighbors = new ArrayList<City>();
        seaNeighbors = new ArrayList<City>();
        airNeighbors = new ArrayList<City>();
        airportsOnFlightPlan = new ArrayList<City>();
    }
    
    public ArrayList<City> getAirPortsForFlightPlan(){
        return this.airportsOnFlightPlan;
    }
    
    public Object clone() throws CloneNotSupportedException{
        City c = new City(this.name, this.color, this.quadrent, this.x, this.y, this.xReal, this.yReal);
        c.landNeighbors.clone();
        c.seaNeighbors.clone();
        return c;
    }
    
    public void turnIntoAirport(){
        this.isAirport = true;
    }
    
    public boolean isThisAnAirport(){
        return this.isAirport;
    }
    public City validateAndUpdateCity(City n){
        try{
            n = (City) n.clone();
        }catch(CloneNotSupportedException e){
            e.printStackTrace();
            System.exit(0);
        }
        int width = ui.paneWidth/2;
        int height = ui.paneHeight;
        int quad = this.quadrent;
        switch(quad){
            case 2:
                this.xReal += width;
                break;
            case 3:
                this.yReal += height;
                break;
            case 4:
                this.xReal += width;
                this.yReal += height;
                break;
        }

        quad = n.quadrent;
        switch(quad){
            case 2:
                n.xReal += width; 
                break;
            case 3:
                n.yReal += height;
                break;
            case 4:
                n.xReal += width;
                n.yReal += height;
                break;
        }

        while(this.xReal > width){
            n.xReal -= width;// - offN[0];
            this.xReal -= width;
        }
        while(this.yReal > height){
            n.yReal -= height;// - offN[1];
            this.yReal -=height;
        }
        return n;
    }
}