package Objects;

public class Point {
    
    public int x;
    public int y;
    public int quad;
    
    public Point(int x, int y, int quad){
        this.x = x;
        this.y = y;
        this.quad = quad;
    }
    
    public Point(int x, int y){
        this.x = x;
        this.y = y;
        quad = -1;
    }
    
    public int hashCode(){
        return (x*1000)+y;
    }
    
    public boolean equals(Object o){
        Point p = (Point)o;
        if(p.x == x && p.y == y && p.quad == quad){
            return true;
        }
        return false;
    }
    
}
