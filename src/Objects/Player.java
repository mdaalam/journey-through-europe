package Objects;

import Application.Main;
import static Application.Variables.*;
import static Application.Main.*;
import JourneyUI.AI;
import JourneyUI.JTEDataManager;
import JourneyUI.JTEUI;
import Objects.Card;
import Objects.City;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import javafx.animation.Animation;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.Transition;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import javax.swing.JEditorPane;

public class Player {
    
    
    public ArrayList<String> history = new ArrayList<String>();
    public static ArrayList<JEditorPane> jEditorObjs = new ArrayList<JEditorPane>();
    
    public boolean human;
    public String flagColor;
    public ImageView ivLabel;
    public ImageView ivGame;
    public ImageView flag;
    public int flagQuadrant;
    public int quadrant = -1;
    
    public ToggleGroup group;
    public Button buttonLabel;
    public String name;
    public Color color;
    public String colorField;
    public TextField textField;
    private boolean hasRolled = false;
    public ArrayList<Card> cards;
    public ArrayList<String> cardNamesToBeLoaded = new ArrayList<String>();

    public BorderPane leftSide; 
    public JTEUI ui;
    public int playerImageWidth;
    public int playerImageHeight;
    
    public City currentCity;
    public City previousCity;
    public City seaRouteCity;
    public ArrayList<City> landCities;
    public ArrayList<City> seaCities;
    public ArrayList<City> airCities;
    public ArrayList<City> shortestPath;
    public double x;
    public double y;
    
    private int rollCount = 0;
    public int initialRollCount = 0;
    
    public String toString(){
        String result = "";
        result += "Player.TOSTRING: " + this.name + " H: " + isHuman() + " C: " + isComputer() + " rolled: " + hasRolled + " moves: " + rollCount + " ";
        result += this.currentCity.name + " ";
        result += ((this.previousCity == null) ? "- " : previousCity.name) + " ";
        return result;
    }
    
    public Player(JTEUI ui){
        this.ui = ui;
        cards = new ArrayList<Card>();
        landCities = new ArrayList<City>();
        seaCities = new ArrayList<City>();
        airCities = new ArrayList<City>();
        shortestPath = new ArrayList<City>();
        
        leftSide = new BorderPane();
        leftSide.setMinSize(ui.paneWidth/2, ui.paneHeight);
        leftSide.setMaxSize(ui.paneWidth/2, ui.paneHeight);
        buttonLabel = new Button();
        buttonLabel.setLayoutX(0);
        buttonLabel.setLayoutY(0);
        human = true;

    }
    
    public int getNumMoves(){
        return rollCount;
    }
    
    /**
     * subtracts one from the number of rolls available
     * @return new decremented roll counter
     */
    public int useMove(){
        --rollCount;
        ui.updateNumMoveText();
        return rollCount;
        
    }
    
    public void zeroOutNumMoves(){
        rollCount = 0;
        ui.updateNumMoveText();
    }
    /**
     * adds to roll;
     * @return the value of newly updated roll counter
     */
    public int addNumMoves(int num){
        rollCount += num;
//        if(seaRouteCity == null){   
            ui.updateNumMoveText();
            initialRollCount = rollCount;
//        }
        return rollCount;
    }
    
    public ArrayList<City> getNeighboringCitiesLand(){
        return this.landCities;
    }
    
    public ArrayList<City> getNeighboringCitiesSea(){
        return this.seaCities;
    }
    
    public boolean moveCurrentCity(City city){
        debugPrint(PlayerMethodCalls, "Moving City - Player ");
        history.add(currentCity.name);
        
        
        boolean found = false;
        
        previousCity = currentCity;
        currentCity= ui.dataManager.nameToCityMapping.get(city.name);
        this.landCities = currentCity.landNeighbors;
        this.seaCities = currentCity.seaNeighbors;
        this.airCities = currentCity.airNeighbors;
        quadrant = currentCity.quadrent;
        if(this.seaCities.contains(city)){
            System.out.println("this cannot be hitting - Player class");
            this.zeroOutNumMoves();
        }else{
            this.useMove();
        }
        
        for(int i = 0; i < cards.size(); i ++){
            if(cards.get(i).name.equals(currentCity.name)){
                found = true;
                leftSideRemoval(i+1);
                if(currentCity == ui.dataManager.nameToCityMapping.get(cards.get(i).name)){
                    history.add(currentCity.name  + " - " + JTEDataManager.cityInfo.get(currentCity.name));
                }
                
                cards.remove(i);
                this.turnComplete();
                
//                if(i == 0){
//                    System.out.println(cards.get(i).name + ".equals(" + currentCity.name + ")");
//                }
            }else{
                if(i == 0){
                    System.out.println(cards.get(i).name + ".equals(" + currentCity.name + ")");
                }
            }
            
        }
        
        return found;
    }
    
    public void turnComplete(){
        
        System.out.println("Turn Complete: " + this.name);
        history.add("\t Turn Ended =)");
        Player player = ui.players.get(ui.getCurrentPlayer());
        
        this.zeroOutNumMoves();
        System.out.println("turn Complete - ClickMovementHandler - //next line//");
        System.out.println("\t turn ending for " + this.name);
        this.hasRolled(false);
        initialRollCount = 0;
        previousCity = null;
        ui.center.getChildren().clear();
        if(this.cards.size() == 0){
            JTEUI.winner = this;
            ui.changeWorkspace("win");
        }else{
            ui.changeToNextPlayer();
            System.out.println("cmh: calling loadPlayersToMap");
            ui.loadPlayersToMap();

            ui.autoRun();
            
            Player p = ui.players.get(ui.getCurrentPlayer());
            if(p.seaRouteCity != null){
                System.out.println("before calling click to map: "
                        + "" + ui.players.get(ui.getCurrentPlayer()).name + " -- "
                        + "" + ui.players.get(ui.getCurrentPlayer()).seaRouteCity);
                p.hasRolled(true);
                p.addNumMoves(1);
//                ui.mch.clickToMap(p.seaRouteCity.xReal, p.seaRouteCity.yReal);
                ui.mch.clickToMap(p.seaRouteCity.xReal, p.seaRouteCity.yReal);
//                p.hasRolled(true);
                
                
                System.out.println("after calling click to map: "
                        + "" + ui.players.get(ui.getCurrentPlayer()).name + " -- "
                        + "" + ui.players.get(ui.getCurrentPlayer()).seaRouteCity);
            }
            
        }
    }
    
    
    private void leftSideRemoval(int i){
        int gap = Integer.MAX_VALUE;
        for(int j = 2; j < leftSide.getChildren().size(); j ++){
            Button a = (Button) leftSide.getChildren().get(j-1);
            Button b = (Button) leftSide.getChildren().get(j);
            int temp = (int)(b.getLayoutY() - a.getLayoutY());
            gap = (temp < gap)? temp: gap;
        }
        leftSide.getChildren().remove(i);
//        i++;
        for(; i < leftSide.getChildren().size(); i ++){
            
            Button b = (Button)leftSide.getChildren().get(i);
//            ImageView iv = (ImageView)b.getGraphic();
//            if(iv == null){
//                System.out.println("i give up");
//            }

            final Duration SEC_1 = Duration.millis(1500);
            Animation animation = new Transition() {
                {
//                System.out.println("starting");
                    setCycleDuration(Duration.millis(1500));
                }

                protected void interpolate(double frac) {
                }
            };

//            animation.play();
            animation.setOnFinished(new EventHandler<ActionEvent>(){
                public int counter = 1;
                @Override
                public void handle(ActionEvent event) {
                    b.setTranslateY(0);
//                    b.setLayoutY(((ui.paneHeight/8)*counter++));
                }
            });

            TranslateTransition tt = new TranslateTransition(new Duration(1500));
           
//            try{
//                tt.setFromY(b.getLayoutY());
//            }catch(NullPointerException e){
//                e.printStackTrace();
//            }
//            tt.setByY(((i)*ui.paneHeight/11)-gap);
            tt.setByY(-150);
            tt.setCycleCount(1);

            ParallelTransition pt = new ParallelTransition(b, tt);

            pt.play();

        }
    }
    
    
//    public  ArrayList<ArrayList<ArrayList<City>>> matrix = new ArrayList<ArrayList<ArrayList<City>>>();
    public ArrayList<Integer> order = new ArrayList<Integer>();
    public boolean fast = false;
    
    public ArrayList<City> getShortestPaths(AI di) {
        ui.wasMoved = 0;
        debugPrintln(DijkstraTesting, "in Player: " + this.name + " getShortestPath");
        ArrayList<City> path = new ArrayList<City>();
        
        City x = ui.dataManager.nameToCityMapping.get(currentCity.name);
        System.out.println(" getShortestPath - " + ((x == null)? "null": x.name) );
        int best = 200;
        ArrayList<String> sp = null;
        ArrayList<String> bestList = null;
        int loc = -1;
        
        if (cards.size() == 1) {
            sp = di.getShortestPathFor( x, ui.dataManager.nameToCityMapping.get(cards.get(0).name));
            System.out.println();
            loc = 0;
            best = sp.size();
        } 
        else {

            for (int i = 1; i < cards.size(); i++) {
                System.out.print("223: ");
                
                City y = ui.dataManager.nameToCityMapping.get(cards.get(i).name);        
                sp = di.getShortestPathFor(x, y);
                if(sp.size() < best && sp.size() > 1){
                    loc = i;
                    best = sp.size();
//                    bestList = sp;
                }else if(sp.size() <= 1){
                    System.out.println("is -1 for " + x.name + " -- " + y.name);
                }else{
                    System.out.println();
                }
                
            }
//            sp = di.getShortestPathFor(x, ui.dataManager.nameToCityMapping.get(cards.get(loc).name));
        }
        System.out.println("sp.size(): " + sp.size() + " best: " + best + " loc: " + loc);
        bestList = di.getShortestPathFor(x, ui.dataManager.nameToCityMapping.get(cards.get(loc).name));
        for(String s: bestList){
            path.add(ui.dataManager.nameToCityMapping.get(s));
        }
        JTEUI.wasMoved = 0;
        return path;
    }


    
        
        
    
    
    public void setPlayerAndImageX(double x){
        this.x = x;
        ivGame.setX(x);
    }
    
    public void setPlayerAndImageY(double y){
        this.y = y;
        ivGame.setY(y);
    }
    
    public boolean addCard(Card c){
        if(c == null){
            return false;
        }
        if(cards.contains(c)){
            return false;
        }
        cards.add(c);
        return true;
    }
    public void setColor(Color color){
        this.color = color;
//        this.color = Color.GREEN;
        
    }
    
    public void setupPlayerImage(){
        String path = "./images/piece_" + this.colorField + ".png";
        ivLabel = null;
        ivGame = null;
        try{
//            Image imlabel = new Image(new File(path).toURI().toURL().toExternalForm());
//            Image im = new Image(new File(path).toURI().toURL().toExternalForm());
            Toolkit tool = Toolkit.getDefaultToolkit();
            Dimension d = tool.getScreenSize();
            int width = d.width - 50;
            int height = d.height - 50;
            playerImageWidth = height/10;
            playerImageHeight = height/10;
            
            ivLabel = new ImageView(new Image(new File(path).toURI().toURL().toExternalForm(), 50, 50, false, false));
            ivGame = new ImageView(new Image(new File(path).toURI().toURL().toExternalForm(), 100, 100, false, false));
            ivGame.setStyle("-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.8), 10, 0, 0, 0);");
//            System.out.println("player image setup (width --> height" + ivLabel.getImage().getWidth() + " --> " + ivLabel.getImage().getHeight());
        }catch(Exception e){
            e.printStackTrace();
            System.exit(0);
        }
    }
    
    public void hasRolled(boolean b){
        hasRolled = b;
    }
    
    public boolean hasRolled(){
        return this.hasRolled;
    }
    
    public String getName(){
        return name;
    }
    public boolean isHuman(){
        return human;
    }
    
    public boolean isComputer(){
        return !human;
    }
    
    public void updatePlayer(int n){
        if(this.textField.getText() == null){
            this.name = "Player " + (n + 1);
        }else if( "".equals(this.textField.getText())){
            this.name = "Player " + (n + 1);
        }else{
            this.name = this.textField.getText();
        }
        this.buttonLabel.setText(name);
    }
    
}
