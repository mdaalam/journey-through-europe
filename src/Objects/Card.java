package Objects;

import javafx.scene.control.Button;
import javafx.scene.image.ImageView;

public class Card {

    public String color;
    public String name;
    public String imagePath;
    public ImageView front;
    public ImageView back;
    public boolean used;
    public Button button;
    public static int numUsed = 0;
    
    public Card(String path, ImageView front, ImageView back) {
        path = "./images/" + path.substring(2);
        this.imagePath = path;
        path = path.substring("./images/".length()-1);
        String [] pathStuff = path.split("/");
        this.color = pathStuff[2];
        this.name = pathStuff[3].substring(0,pathStuff[3].indexOf(".jpg")); 
        this.front = front;
        this.back = back;
        button = new Button();
        button.setGraphic(front);
    }
     
    public int hashCode(){
        return name.hashCode();
    }
    
    public boolean equals(Object o){
        Card temp = (Card) o;
        System.out.println("temp." +temp.getCardPath()+ ".equals(" +imagePath+ ") --> " + temp.getCardPath().equals(imagePath));
        if(temp.getCardPath().equals(name)){
            return true;
        }
        return false;
    }

    public String getCardPath(){
        return this.imagePath;
    }

    public String getCardName(){
        return name;
    }

    public String getCardColor(){
        return color;
    }
}