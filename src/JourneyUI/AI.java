package JourneyUI;

import Objects.*;
import static Application.Variables.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;

class Vertex implements Comparable<Vertex>
{
    public final String name;
    public Edge[] adj;
    public double minDistance = Double.POSITIVE_INFINITY;
    public Vertex previous;
    private HashMap<String, Vertex> map = new HashMap<String, Vertex>();

    public Vertex(String argName) {
        name = argName;
    }

    public String toString() {
        return name;
    }

    public Edge [] getAdjacencies(){
        return this.adj;
    }
    public void addAdjacencies(Edge e){
        if(adj == null){
            adj = new Edge[1];
            adj[0] = e;
            return;
        }
        int loc = adj.length;
        Edge[] a = new Edge[loc + 1];
        for(int i = 0; i < adj.length; i ++){
            a[i] = adj[i];
        }
        adj = a;
        adj[loc] = e;
        
    }
    public int compareTo(Vertex other) {
        return Double.compare(minDistance, other.minDistance);
    }
}

class Edge {

    public final Vertex target;
    public final double weight;

    public Edge(Vertex argTarget, double argWeight) {
        target = argTarget;
        weight = argWeight;
    }
}

public class AI {

    private JTEUI ui;
    private static ArrayList<Vertex> allVerticies;
    public HashMap<String, Vertex> map;
        
    public AI(JTEUI ui) {
        this.ui = ui;
        allVerticies = new ArrayList<Vertex>();
        map  = new HashMap<String, Vertex>();
        ArrayList<City> cities = ui.dataManager.allCities;
        
        for(City c : cities){
            Vertex v = new Vertex(c.name);
            allVerticies.add(v);
            map.put(c.name, v);
        }

        for(City c: cities){
            ArrayList<City> land = c.landNeighbors;
            ArrayList<City> sea = c.seaNeighbors;
            ArrayList<City> air = c.airNeighbors;
            
            for(City x : land){
                map.get(c.name).addAdjacencies(new Edge(map.get(x.name), 1));
                map.get(x.name).addAdjacencies(new Edge(map.get(c.name), 1));
            }
            
            for(City x : sea){
                map.get(c.name).addAdjacencies(new Edge(map.get(x.name), 6));
                map.get(x.name).addAdjacencies(new Edge(map.get(c.name), 6));
            }
            
            for(City x : air){
                int cnum = c.airportQuadrent;
                int xnum = x.airportQuadrent;
                
                if(cnum %2 == 0){
                    if(cnum - 1 == xnum || cnum - 2 == xnum || cnum + 2 == xnum){
                        map.get(c.name).addAdjacencies((new Edge(map.get(x.name), 4)));
                    }else if(cnum == xnum){
                        map.get(c.name).addAdjacencies((new Edge(map.get(x.name), 2)));
                    }
                }else{
                    if(cnum + 1 == xnum || cnum - 2 == xnum || cnum + 2 == xnum){
                        map.get(c.name).addAdjacencies((new Edge(map.get(x.name), 4)));
                    }else if(cnum == xnum){
                        map.get(c.name).addAdjacencies((new Edge(map.get(x.name), 2)));
                    }
                }
            }
        }
        
    }
    
    public ArrayList<String> getShortestPathFor(City start, City target){
        debugPrintln(DijkstraTesting, "in getShortestPathFor( " + start.name + ", " + target.name + ")");
        
        Vertex a = map.get(start.name);
        Vertex b = map.get(target.name);
        
        
        for(Vertex v: allVerticies){
            v.minDistance = Double.POSITIVE_INFINITY;
            v.previous = null;
        }
        computePaths(a);
        List<Vertex> path = getShortestPathTo(b);
        ArrayList<String> stringPath = new ArrayList<String>();
        
        for(Vertex v: path){
            stringPath.add(v.name);
        }
        return stringPath;
    }

    public static Vertex oldVertex = null;
    public static void computePaths(Vertex source) {
        debugPrintln(DijkstraTesting, "in computePaths(" + source.name + ")");
        source.minDistance = 0.;
        PriorityQueue<Vertex> vertexQueue = new PriorityQueue<Vertex>();
        vertexQueue.add(source);
        while ( ! vertexQueue.isEmpty()) {
            Vertex u = vertexQueue.poll();
            if(u.adj != null)
            for (Edge e : u.adj) {
                Vertex v = e.target;
                double weight = e.weight;
                double distanceThroughU = u.minDistance + weight;
                if (distanceThroughU < v.minDistance) {
                    vertexQueue.remove(v);
                    v.minDistance = distanceThroughU;
                    v.previous = u;
                    vertexQueue.add(v);
                }
            }
        }
    }

    public static ArrayList<Vertex> getShortestPathTo(Vertex target) {
        ArrayList<Vertex> path = new ArrayList<Vertex>();
        for (Vertex vertex = target; vertex != null; vertex = vertex.previous) {
            path.add(vertex);
        }
        
        Collections.reverse(path);
        return path;
    }
}