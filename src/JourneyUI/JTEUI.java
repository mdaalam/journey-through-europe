package JourneyUI;

import Application.Main;
import static Application.Variables.*;
import Handlers.*;
import static Handlers.MapCitiesHandler.debugPrint;
import static Handlers.MapCitiesHandler.debugPrintln;
import Objects.*;
import Objects.Player;
import java.awt.Dimension;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.concurrent.locks.ReentrantLock;
import javafx.animation.Animation;
import javafx.animation.ParallelTransition;
import javafx.animation.PauseTransition;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Transition;
import javafx.animation.TranslateTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextBoundsType;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;

public class JTEUI extends Pane {

    public static boolean isLoading = false;

    public static ArrayList<JEditorPane> historyBoxes = new ArrayList<JEditorPane>();
    public static Player winner;
    // mainStage
    private Stage primaryStage;

    public final ReentrantLock lock = new ReentrantLock();
    //statics
    public static String[] colorStrings = {"black", "red", "yellow", "green", "blue", "white"};
    public static Color[] colorReal = {Color.BLACK, Color.RED, Color.YELLOW, Color.GREEN, Color.BLUE, Color.WHITE};
    public static int lastCardGiven = 2;

    // mainPane
    private BorderPane mainPane;
    private Pane splashPane;
    public BorderPane playerPane;
    private BorderPane gamePane;
    private BorderPane historyPane;
    private Pane flightPane;
    private BorderPane aboutPane;
    private Pane loadPane;

    //DataManager
    public JTEDataManager dataManager;

    //About Page
    public static WebView browser = new WebView();
    public static WebEngine webEngine = browser.getEngine();

    public static ArrayList<Player> players = new ArrayList<Player>();

    public GridPane playerSelectionboxs = new GridPane();

    public static int currentDieNumber = 1;
    public static ImageView[] dice = new ImageView[7];
    Button die;
    //pos 0 = the letter
    //pos 1 = the number

    public static char[] region = new char[2];
    public static int threshold = 20; // threshold to click on a city
    public static int currentMapSection = 1;

    //New HANDLERS
    public SplashScreenHandlers ssh;//= new SplashScreenHandlers(this)
    public DieHandler dieHandler;
    public MapCitiesHandler mch;
    public PlayerDragHandler pdh;
    public PlayerMousePressedHandler pmph;
    public PlayerMouseReleaseHandler pmrh;

    // SplashScreen
    private ImageView splashScreenImageView;

    private Label splashScreenImageLabel;
    private VBox splashScreenMenu;
    private int splashScreenButtonWidth;
    private int splashScreenButtonHeight;

    //Handlers
    public GoButtonHandler goButtonHandler;
    public AnimationOnFinishedHandler aofh;

    // GamePane
    public Text playerTurnLabel = new Text("");
    public Text rollLabel = new Text("");
//    public Text rollBottomLabel = new Text("");
    public HBox playerTurnAndPeiceHBox;
    public VBox rolledTextVBox = new VBox();

    public HBox playerPaneTop;

    private static int currentPlayer = 0;

    public static int numPlayers = 0;

//    public static int numPlayers = 0;
    public static int numComputers = 0;

    // GRID Renderer
//    public GridRenderer gridRenderer;
//    private GraphicsContext gc;
    private ScrollPane statsScrollPane;
    public JEditorPane statsPane;

    //HelpPane
    private BorderPane helpPanel;
    private JScrollPane helpScrollPane;
    private JEditorPane helpPane;
    private Button homeButton;
    private Pane workspace;

    // Padding
    private Insets marginlessInsets;

    // Image path
//    private String ImgPath = "file:images/";
    // mainPane weight && height
    public int paneWidth;
    public int paneHeight;

    //GameScreen
    public VBox rightSide;

    public Pane center = new Pane();
    public VBox history = new VBox();
//    public static Button [] LeftPlayerLabel = new Button[6];

    // THIS CLASS WILL HANDLE ALL ACTION EVENTS FOR THIS PROGRAM
//    private SokobanEventHandler eventHandler;
//    private SokobanErrorHandler errorHandler;
//    private SokobanDocumentManager docManager;
//    SokobanGameStateManager gsm;
    public AI d;

    public JTEUI() {

//        d  = new AI(this);
        initGameHandlers();

        initMainPane();

        initSplashScreen();

        initPlayerSetupScreen();

//        initAboutScreen();
//        initHistoryScreen();
        initLoadScreen();
//        if( ! isLoading){
            dataManager = new JTEDataManager(this);
//        }
        initGamePlayScreen();

    }

    public void loadFile() {

        isLoading = true;

        
        if(dataManager == null){
            dataManager = new JTEDataManager(this);
        }
        File file = new File("./misc/game.txt");
        Scanner sc = null;//
        
        try {
            sc = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        
//        players = new ArrayList<Player>();
        
        numPlayers = Integer.parseInt(sc.nextLine().trim());
        currentPlayer = sc.nextInt();
        int remainingPointsForPlayer = sc.nextInt();
        boolean haveRolled = sc.nextBoolean();//Line().equals("true")? true: false;
        int initialRollCount = sc.nextInt();
        
//        for(int i = 0; i < 6; i ++){
            players.add(new Player(this));
//        }
        
        for(int i = 0; i < numPlayers; i ++){
            Player p = players.get(i);
            p.name = sc.nextLine().trim();
            System.out.println("check point 1");
            p.currentCity = dataManager.nameToCityMapping.get(sc.nextLine().trim());
            String nextCity = sc.nextLine().trim();
            if(nextCity.equals("-")){
                p.previousCity = null;
            }else{
                p.previousCity = dataManager.nameToCityMapping.get(sc.nextLine().trim());
            }
            
            int numCards = sc.nextInt();
//            p.cards = new ArrayList<Card>();
            
            
            
            
            String next = "";
            while( ! next.equals("|")){
                p.cards.add(dataManager.cardsMap.get(next));
                next = sc.nextLine().trim();
            }
            while(sc.hasNextLine()){
                p.history.add(sc.nextLine());
            }
            
            int yoffset = 200;
            int counta = 0;
            for(Card c : p.cards){
//                Button b = be
//                c. = new Button();
                Button b = c.button;
                b.setGraphic(c.front);
                c.button.setLayoutY(yoffset + (150*counta));
                p.leftSide.getChildren().add(c.button);
            }
        }
        
            initHistoryScreen();
            initFlightScreen();
            continueRunningGame();
        
        
    }

    public void saveFile() {
        System.out.println("in saveFile()");
        File file = file = new File("./mise/game.txt");

        PrintWriter pw = null;
        try {
            pw = new PrintWriter(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Player p = players.get(getCurrentPlayer());
        pw.println(numPlayers);
        pw.println(getCurrentPlayer());
        pw.println(p.getNumMoves());
        pw.println(p.hasRolled());
        pw.println(p.initialRollCount);
        for (int i = 0; i < numPlayers; i++) {
            pw.println(p.name);
            pw.println(p.currentCity.name);
            if (p.previousCity == null) {
                pw.println("-");
            } else {
                pw.println(p.previousCity.name);
            }

            ArrayList<Card> list = p.cards;
            pw.println(list.size());
            for (int j = 0; j < list.size(); j++) {
                pw.println(list.get(j).name);
            }

        }
        pw.flush();
        pw.close();
    }

    public void initMainPane() {
        marginlessInsets = new Insets(0, 0, 0, 0);
        mainPane = new BorderPane();

//        Toolkit tool = Toolkit.getDefaultToolkit();
//        Dimension d = tool.getScreenSize();
        paneWidth = 1870;//d.width - 50;
        paneHeight = 1030;//d.height - 50;
//        System.out.println("paneWidth: " + paneWidth);
//        System.out.println("paneHeight: " + paneHeight);
//        paneHeigth += LEVEL_IMAGE_HEIGHT_WIDTH;
        mainPane.resize(1870, 1030);
        mainPane.setPadding(marginlessInsets);
    }

    public void initPlayerSetupScreen() {
        for (int i = 0; i < 6; i++) {
            players.add(new Player(this));
        }

        playerPane = new BorderPane();
        playerPane.setStyle("-fx-background-color: orange;");//rgb(100, 100, 100);");
//        playerPane.setPrefSize(paneWidth, paneHeight);
//        playerPane.setBackground(new Background(new BackgroundFill(Color.ORANGE, CornerRadii.EMPTY, Insets.EMPTY)));
        playerPaneTop = new HBox();
        Label l = new Label("Number of players: ");
        ObservableList<String> options
                = FXCollections.observableArrayList(
                        "1",
                        "2",
                        "3",
                        "4",
                        "5",
                        "6"
                );
        ComboBox cb = new ComboBox(options);
        Button b = new Button("GO!");

        b.setOnAction(goButtonHandler);
//        b.setPrefSize(20, 20);

        StackPane stackpane = new StackPane();
        StackPane.setAlignment(l, Pos.CENTER_RIGHT);
        stackpane.getChildren().add(l);
//        box.getChildren().get(2).set;
        playerPaneTop.getChildren().add(stackpane);
        playerPaneTop.getChildren().add(cb);
        playerPaneTop.getChildren().add(b);
//        StackPane.setAlignment(title, Pos.BOTTOM_CENTER);
        playerPaneTop.setSpacing(10);
        playerPaneTop.setMinWidth(300);
        playerPaneTop.setStyle("-fx-background-color: orange;");

        Insets in = new Insets(20);
        playerSelectionboxs.setPadding(in);
        playerSelectionboxs.setPrefSize(paneWidth, paneHeight);

        for (int i = 0; i++ < 3;) {
            playerSelectionboxs.getColumnConstraints().add(new ColumnConstraints((paneHeight - 100) / 2));
            if (i < 2) {
                playerSelectionboxs.getRowConstraints().add(new RowConstraints((paneHeight - 100) / 2));
            }
        }

        numPlayers = 0;
        populatePlayerChoices(playerChoices, 2);
        numPlayers = 2;

        cb.setOnAction((event) -> {

            int selected = Integer.parseInt(cb.getSelectionModel().getSelectedItem().toString());
            idcMakeStuffWork(selected);
//          
        });

        Rectangle rect = new Rectangle(paneWidth, paneHeight);
        rect.setFill(Color.ORANGE);
        playerPane.getChildren().add(rect);
        playerPane.setCenter(playerSelectionboxs);
        playerPane.setTop(playerPaneTop);

    }

    public static ArrayList<FlowPane> playerChoices = new ArrayList<FlowPane>();

    public void idcMakeStuffWork(int selected) {
        System.out.println("number selected: " + selected + " --> " + numPlayers);
        populatePlayerChoices(playerChoices, selected);

        if (numPlayers > selected) {

            for (int i = numPlayers; i > selected; i--) {
                playerSelectionboxs.getChildren().remove(playerChoices.get(playerChoices.size() - 1));
                playerChoices.remove(playerChoices.size() - 1);
            }
        }

        numPlayers = selected;
    }

    public void populatePlayerChoices(ArrayList<FlowPane> list, int choice) {
        if (choice > numPlayers) {

//            System.out.println("in popultePlayerChoices(" + list.size() +", " + choice + ")");
            for (int i = numPlayers + 1; i <= choice; i++) {
                Player p = players.get(i - 1);
                FlowPane fp = new FlowPane();
                int size = (paneHeight - 100) / 2;
//                gp.setPrefSize(size, size);
                fp.setMinSize(size, size);
                fp.setStyle("-fx-border: 2px solid; -fx-border-color: black; -fx-background-color: orange;");
                fp.setHgap(10);

                Label name = new Label("Name: ");
                ImageView iv = null;
                String flagPath = "./images/flag_";

                final ToggleGroup group = new ToggleGroup();

                p.group = group;

                RadioButton rb1 = new RadioButton("Player");
                rb1.setToggleGroup(group);
                rb1.setSelected(true);

                RadioButton rb2 = new RadioButton("Computer");
                rb2.setToggleGroup(group);

                group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
                    @Override
                    public void changed(ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) {
                        if (group.getSelectedToggle() != null) {

                            for (Player person : players) {
                                if (person.group == group) {
                                    person.human = (group.getSelectedToggle().toString().toLowerCase().contains("computer"))
                                            ? false : true;
//                                    System.out.println("toggle group for " + person.name + " switched to " + (person.human ? "human":"computer"));
                                }
                            }

                        }
                    }

                });

                VBox radioButtons = new VBox();
                radioButtons.setMinWidth(100);
                radioButtons.getChildren().add(rb1);
                radioButtons.getChildren().add(rb2);

                Label label1 = new Label("Name: ");
                TextField nameTextField = new TextField();
                p.textField = nameTextField;
                VBox nameBox = new VBox();
                String color = "";
                nameBox.getChildren().addAll(label1, nameTextField);
                nameBox.setSpacing(10);
                Color playerColor = Color.BEIGE;

//                flagPath += colorStrings[i-1] + ".png";
//                color = colorStrings[i-1];
//                playerColor = colorReal[i-1];
                switch (i) {
                    case 1:
                        flagPath += "black.png";
                        color = "black";
                        playerColor = Color.BLACK;
                        break;
                    case 2:
                        flagPath += "red.png";
                        color = "red";
                        playerColor = Color.RED;
                        break;
                    case 3:
                        flagPath += "yellow.png";
                        color = "yellow";
                        playerColor = Color.YELLOW;
                        break;
                    case 4:
                        flagPath += "green.png";
                        color = "green";
                        playerColor = Color.GREEN;
                        break;
                    case 5:
                        flagPath += "blue.png";
                        color = "blue";
                        playerColor = Color.BLUE;
                        break;
                    case 6:
                        flagPath += "white.png";
                        color = "white";
                        playerColor = Color.WHITE;
                        break;

                }
                try {
                    iv = new ImageView(new Image((new File(flagPath)).toURI().toURL().toExternalForm()));
//                    p.colorField = new ImageView(loadImage("./images/piece_" + color + ".png", paneHeight/22, paneHeight/22));
                    p.colorField = color;
                    p.flag = iv;
                    p.setColor(playerColor);
//                    p.color = playerColor;
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(0);
                }
                HBox container = new HBox();
                container.setPrefSize((paneHeight - 100) / 2 - 100, 300);
                container.setStyle("-fx-border: 5px solid; -fx-border-color: red;");

                StackPane stackpane = new StackPane();
                StackPane.setAlignment(iv, Pos.CENTER_LEFT);
                stackpane.getChildren().add(iv);

                container.getChildren().add(stackpane);

                stackpane = new StackPane();
                StackPane.setAlignment(radioButtons, Pos.CENTER);
                stackpane.getChildren().add(radioButtons);

                container.getChildren().add(stackpane);

                stackpane = new StackPane();
                StackPane.setAlignment(nameBox, Pos.CENTER_RIGHT);
                stackpane.getChildren().add(nameBox);
                container.getChildren().add(stackpane);

//                stackpane = new StackPane();
//                StackPane.setAlignment(container, Pos.CENTER);
//                stackpane.getChildren().add(container);
                container.setAlignment(Pos.CENTER);

                fp.getChildren().add(container);

                list.add(fp);
//                playerSelectionboxs.getChildren().add(gp);
                playerSelectionboxs.add(fp, (i - 1) / 2, (i + 1) % 2);
            }
        }

    }

    public void initGameHandlers() {
        dieHandler = new DieHandler(this);
        goButtonHandler = new GoButtonHandler(this);
        mch = new MapCitiesHandler(this);
        aofh = new AnimationOnFinishedHandler(this);
        pdh = new PlayerDragHandler(this);
        pmph = new PlayerMousePressedHandler(this);
        pmrh = new PlayerMouseReleaseHandler(this);
    }

    public void initAboutScreen() {
        final Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(getPrimaryStage());
        BorderPane modal = new BorderPane();
        Scene stfu = new Scene(modal, paneWidth - 200, paneHeight - 100);
        dialogStage.setScene(stfu);

        aboutPane = new BorderPane();
        Rectangle rect = new Rectangle(paneWidth - 200, 200);
        rect.setFill(Color.rgb(200, 200, 200));

        webEngine.load("http://en.wikipedia.org/wiki/Journey_Through_Europe");
        aboutPane.setCenter(browser);

        FlowPane tempPane = new FlowPane();
        tempPane.setMinSize(paneWidth - 300, 200);
        tempPane.setMaxSize(paneWidth - 300, 200);

//        tempPane.getChildren().add(rect);
        StackPane s = new StackPane();
        HBox box = new HBox();
        box.setAlignment(Pos.CENTER);
        box.setPadding(new Insets(50, 50, 50, 50));

        Label contact = new Label("Contact Info");
        contact.setMinWidth(((paneWidth - 200) * 0.2));
        contact.setFont(new Font("Verdana", 24));
        contact.setStyle("-fx-fill: green");

        Label name = new Label("Name: Md Alam");
        name.setMinWidth(((paneWidth - 200) * 0.2));
        name.setFont(new Font("Verdana", 20));
        name.setStyle("-fx-fill: green");

        Label email = new Label("Email: iamarfad@yahoo.com");
        email.setMinWidth(((paneWidth - 200) * 0.2));
        email.setFont(new Font("Verdana", 20));
        email.setStyle("-fx-fill: green");

        Label phoneNumber = new Label("Cell: (718)825-6546");
        phoneNumber.setMinWidth(((paneWidth - 200) * 0.2));
        phoneNumber.setFont(new Font("Verdana", 20));
        phoneNumber.setStyle("-fx-fill: green");

        box.setAlignment(Pos.CENTER);
        box.getChildren().add(name);
        box.getChildren().add(email);
        box.getChildren().add(phoneNumber);

        tempPane.setPadding(new Insets(50, 50, 50, 50));
        box.setLayoutX(100);
        box.setLayoutY(10);
        tempPane.getChildren().add(box);

        aboutPane.setBottom(tempPane);

        modal.setCenter(aboutPane);

        dialogStage.show();

    }

    public void flightPlanClicked() {
        Player p = players.get(getCurrentPlayer());
        if (p.getNumMoves() <= 0 && !p.hasRolled()) {
            loadFailureToRollDialogBox();
            return;
        }
        System.out.println("flight plan clicked on");

        final Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(getPrimaryStage());
        BorderPane modal = new BorderPane();
        Pane pane = new Pane();

//            ImageView iv = new
        City curr = p.currentCity;

        Circle c = new Circle(20);
        c.setCenterX(curr.airPointCoords.x);
        c.setCenterY(curr.airPointCoords.y);
        c.setStroke(Color.WHITE);
        c.setStrokeWidth(5);
        pane.getChildren().add(flightPane);
        pane.getChildren().add(c);
//            pane.getChildren().add(d);
        modal.getChildren().add(pane);
        Scene scene2 = new Scene(modal, 560, 616);
        dialogStage.setScene(scene2);
        dialogStage.show();
        //                Button noButton = new Button("");
    }

    public void initFlightScreen() {
        System.out.println("initalizing flight screen - initFlightScreen");
        this.dataManager.importAirportInformation();
        flightPane = new Pane();
        ImageView iv = null;
        try {
            iv = new ImageView(new Image(new File("./images/Fligh_Plan.jpg").toURI().toURL().toExternalForm(), 560, 616, true, false));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        System.out.println("image size: " + iv.getImage().getWidth() + " -- " + iv.getImage().getHeight());
        flightPane.getChildren().add(iv);

        flightPane.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                int x = (int) event.getX();
                int y = (int) event.getY();
                x = 20 * (x / 20);
                y = 20 * (y / 20);
                x += 10;
                y += 10;
                flightPlanClickLoopHole(x, y);
            }
        });
    }

    public void flightPlanClickLoopHole(int x, int y) {
        System.out.println("in flightPlanClickLoopHole");

        Player p = players.get(getCurrentPlayer());
        ArrayList<City> list = dataManager.airportList;
        int numMovesToUse = 0;
        for (City c : list) {
            if (c.airPointCoords.x == x && c.airPointCoords.y == y) {
                System.out.println("you have clicked on: " + c.name);
                if (p.getNumMoves() >= 2) {
                    System.out.println("p.getNumMoves() >= 2");
                    if (p.currentCity.airportQuadrent == c.airportQuadrent) {
                        numMovesToUse = 2;
                    } else {
                        if (p.getNumMoves() >= 4) {
                            numMovesToUse = 4;
                        }
                    }
                    if (numMovesToUse > 0) {
                        numMovesToUse--;
                        while (numMovesToUse-- > 0) {
                            p.useMove();
                        }
                        Animation animation = new Transition() {
                            {
                                setCycleDuration(Duration.millis(1000));//Duration.millis((comp) ? 2000 : 1000));
                            }

                            protected void interpolate(double frac) {
                            }
                        };

                        TranslateTransition tt = null;
                        if (false) {
                            debugPrint(ComputerTesting, "COMPUTER - setting delays for animation and transition");
                            tt = new TranslateTransition(new Duration(2000), p.ivGame);
                            tt.setDelay(Duration.millis(1000));

                            animation.setDelay(Duration.millis(1000));
                        } else {

                            debugPrint(ComputerTesting, "HUMAN - creating delay for human");
                            changeWorkspace(c.quadrent);
                            tt = new TranslateTransition(new Duration(Main.CardLoadSpeed), p.ivGame);
                            //                    tt.setAutoReverse(true);
                        }

                        Point target = new Point(c.xReal - p.currentCity.xReal, c.yReal - p.currentCity.yReal);
                        //                Point target = new Point(selectedCity.xReal - (int)player.ivGame.getImage().getWidth()/2, selectedCity.yReal - (int)player.ivGame.getImage().getHeight());
                        p.ivGame.setDisable(true);

                        center.getChildren().remove(p.ivGame);
                        center.getChildren().add(p.ivGame);

                        debugPrintln(mapClickTrace, "\t mapCitiesHandler - movePlayerToCityCalled");

                        //--------------------------------------------------------------------------
                        movePlayerToCity(p, c, target, tt, animation, false);

                    }
                }

            }
        }
    }

    public void initHistoryScreen() {
        historyPane = new BorderPane();
        historyPane.setPadding(new Insets(0, 0, 0, 0));
        Rectangle rect = new Rectangle(paneWidth * 0.75, paneHeight);
        rect.setFill(Color.rgb(200, 200, 200));
        historyPane.getChildren().add(rect);

        int fpWidth = (int) ((paneWidth * 0.75));
        int fpHeight = paneHeight;

        System.out.println("paneWidth: " + paneWidth);
        System.out.println("fpWidth: " + fpWidth);
        System.out.println("sectionWidth: " + fpWidth / 2);

        FlowPane fp = new FlowPane();
        fp.setMinSize(fpWidth, fpHeight);
        fp.setMaxSize(fpWidth, fpHeight);

        ArrayList<Pane> boxes = new ArrayList<Pane>();

        HBox top = new HBox();
        HBox bottom = new HBox();
        VBox stuff = new VBox();

        for (int i = 0; i < numPlayers; i++) {
            int sectionWidth = fpWidth / ((numPlayers > 3) ? 3 : numPlayers);
            int sectionHeight = fpHeight / ((numPlayers > 3) ? 2 : 1);

            Pane hb = new Pane();

            if (i < 3) {
                top.getChildren().add(hb);
            } else {
                bottom.getChildren().add(hb);
            }

//            boxes.add(hb);
            hb.setMinSize(sectionWidth, sectionHeight);
            hb.setMaxSize(sectionWidth, sectionHeight);
            hb.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.EMPTY, new Insets(0, 0, 0, 0))));
            hb.setLayoutX(sectionWidth * i);

            System.out.println("sectionWidth * i: " + sectionWidth + " * " + i + " -> " + sectionWidth * i);
            hb.setLayoutY(sectionHeight * i);

            hb.setBackground(new Background(new BackgroundFill(colorReal[i], CornerRadii.EMPTY, new Insets(0, 0, 0, 0))));

            VBox container = new VBox();
            container.setMinSize(sectionWidth, sectionHeight);
            container.setMaxSize(sectionWidth, sectionHeight);

            Label label = new Label();
            if (players.get(i) == null || players.get(i).name == null) {
                label.setText(players.get(i).name);
            } else {
                label.setText("Player " + (i + 1) + ":");
            }
            label.setFont(new Font("Verdana", 20));
            label.setStyle("-fx-fill: " + colorStrings[i] + "; fx-stroke: black; -fx-stroke-width: 1;");
            label.setMinWidth(sectionWidth);
            label.setMaxWidth(sectionWidth);
            label.setMinHeight(sectionHeight / 5);
            label.setMaxHeight(sectionHeight / 5);
            label.setTextAlignment(TextAlignment.CENTER);
            label.setAlignment(Pos.CENTER);

            JEditorPane jp = new JEditorPane();
            jp.setEditable(false);
            jp.setMinimumSize(new Dimension(sectionWidth, 4 * sectionHeight / 5));
            SwingNode swingNode = new SwingNode();
            JScrollPane jsp = new JScrollPane(jp);
            ScrollPane helpScrollPaneFX = new ScrollPane();
            swingNode.setContent(jsp);
            helpScrollPaneFX.setContent(swingNode);
            helpScrollPaneFX.setFitToHeight(true);
            helpScrollPaneFX.setFitToWidth(true);

            historyBoxes.add(jp);

            container.getChildren().add(label);
            container.getChildren().add(helpScrollPaneFX);

            hb.getChildren().add(container);
        }
        stuff.getChildren().add(top);
        stuff.getChildren().add(bottom);
        historyPane.getChildren().add(stuff);

//        Label l = new Label("Sorry you have no history yet =P");
//        l.setFont(new Font("Comic Sans", 45));
//        l.setStyle("-fx-fill: black");
//
//        VBox idc = new VBox();
//        idc.setMinSize(paneWidth * 0.75, 200);
//        idc.setStyle("-fx-background-color: white;");
//
//
//        StackPane.setAlignment(l, Pos.CENTER);
//        StackPane s = new StackPane();
//        s.setMinSize(paneWidth * 0.75, 200);
//        s.getChildren().add(l);
//        idc.getChildren().add(s);
//        historyPane.setTop(s);
    }

    public void initLoadScreen() {
        System.out.println("initLoadScreen not complete");
    }

    public void initSplashScreen() {
        mainPane.getChildren().clear();
        // INIT THE SPLASH SCREEN CONTROLS
        String splashScreenImagePath = "./images/Game.jpg";
//        String str = props.getProperty(SokobanPropertyType.INSETS);

        splashPane = new FlowPane();

        Image splashScreenImage = loadImage(splashScreenImagePath, paneWidth, paneHeight);
        splashScreenImageView = new ImageView(splashScreenImage);

        splashScreenImageLabel = new Label();
        splashScreenImageLabel.setGraphic(splashScreenImageView);
        // move the label position to fix the pane
        splashScreenImageLabel.setLayoutX(-45);
        splashPane.getChildren().add(splashScreenImageLabel);

        splashScreenMenu = new VBox();
        splashScreenMenu.setSpacing(10.0);
        splashScreenMenu.setAlignment(Pos.CENTER);
        splashScreenButtonWidth = paneWidth / 10;
        splashScreenButtonHeight = splashScreenButtonWidth / 2;
        // add key listener

        String buttonPaths = "";
        String path = "./images/";
        String buttonName = "";
        for (int i = 0; i < 4; i++) {
            switch (i) {
                case 0:
                    buttonName = "startButton.jpg";
                    buttonPaths = path + buttonName;
                    break;
                case 1:
                    buttonName = "aboutButton.jpg";
                    buttonPaths = path + buttonName;
                    break;
                case 2:
                    buttonName = "loadButton.jpg";
                    buttonPaths = path + buttonName;
                    break;
                case 3:
                    buttonName = "quitButton.jpg";
                    buttonPaths = path + buttonName;
                    break;
            }

//            Image levelImage = loadImage(buttonPaths);
            Image levelImage = loadImage(buttonPaths, splashScreenButtonWidth, splashScreenButtonHeight);

            ImageView levelImageView = new ImageView(levelImage);

            // AND BUILD THE BUTTON
            Button levelButton = new Button(null, levelImageView);
            ssh = new SplashScreenHandlers(this, buttonName);

            // CONNECT THE BUTTON TO THE EVENT HANDLER
            levelButton.setOnMouseClicked(ssh);

            splashScreenMenu.getChildren().add(levelButton);

        }

        splashScreenMenu.setLayoutX((paneWidth + splashScreenButtonWidth) / 2);
        splashScreenMenu.setLayoutY((paneHeight / 2) + splashScreenButtonHeight * 2);

        mainPane.setCenter(splashPane);
        mainPane.getChildren().add(splashScreenMenu);
    }

    public void consolidatePlayerSetupScreen() {
        if (isLoading) {

        } else {
            for (int i = 0; i < numPlayers; i++) {
                Player p = players.get(i);
                p.updatePlayer(i);
                p.setupPlayerImage();

            }

            Player p = players.get(currentPlayer);

            playerTurnLabel.setText(p.name + " Turn ");
            playerTurnAndPeiceHBox.getChildren().clear();
            playerTurnAndPeiceHBox.getChildren().add(playerTurnLabel);
            ImageView iv = new ImageView(p.ivLabel.getImage());
            playerTurnAndPeiceHBox.getChildren().add(iv);

            updateTextOnRoll();
        }
    }

    public void updateTextOnRoll() {

        Player p = players.get(currentPlayer);

        rolledTextVBox.getChildren().clear();
        if (p.hasRolled() || p.getNumMoves() > 0) {

            rollLabel.setText("Rolled " + currentDieNumber + "\n"
                    + "Select City");

        } else {
            rollLabel.setText("Please click on the\n"
                    + "die to roll");
        }
        rollLabel.setFont(Font.font("Verdana", 20));
        rollLabel.setFill(p.color);

        rolledTextVBox.getChildren().add(rollLabel);

        updateRightSidePlayerInfo();

    }

    public void updateRightSidePlayerInfo() {
        Player p = players.get(this.getCurrentPlayer());
        playerTurnAndPeiceHBox.getChildren().clear();

        playerTurnLabel = new Text(p.getName() + " ");
        playerTurnLabel.setFont(Font.font("Verdana", 20));
        playerTurnLabel.setFill(p.color);
        HBox temp = new HBox();
//        temp.setMinWidth(paneWidth/4);
        temp.setAlignment(Pos.CENTER);
        temp.getChildren().add(playerTurnLabel);
        p.ivLabel.setFitWidth(50);
        p.ivLabel.setFitHeight(50);

        temp.getChildren().add(p.ivLabel);

        playerTurnAndPeiceHBox.getChildren().add(temp);
    }

    public void initGamePlayScreen() {
        int w = paneWidth / 4;
        int h = paneHeight;

        dataManager.importCards();

        for (int i = 1; i < dice.length; i++) {
            String temp = "./images/die_" + i + ".jpg";
            dice[i] = new ImageView(loadImage(temp, (int) (w / 2.5), (int) (w / 2.5)));
        }

//        mainPane.getChildren().clear();
        gamePane = new BorderPane();
        gamePane.setPrefSize(paneWidth, paneHeight);

        history.setSpacing(10);

        //setup 
        for (int i = 0; i < players.size(); i++) {
            Player p = players.get(i);
//            players.get(i).leftSide= new VBox();
            p.leftSide.setMinSize(w, h);
            p.leftSide.setMaxSize(w, h);
//            players.get(i).leftSide.setPadding(new Insets(h/22.0, 0, 0, 0));

            p.buttonLabel.setFont(new Font("Verdana", 20));
            p.buttonLabel.setDisable(true);
            p.buttonLabel.setStyle("-fx-fill: " + colorStrings[i] + ";");
            p.buttonLabel.setMinSize(w, h / 11);
            p.buttonLabel.setMaxSize(w, h / 11);
//            LeftPlayerLabel[i] = new Button("Player " + (i + 1));
//            LeftPlayerLabel[i].setFont(new Font("Verdana", 20));
//            LeftPlayerLabel[i].setStyle("-fx-fill: " + colorStrings[i] +";");
//            LeftPlayerLabel[i].setPrefSize(w, h/11);
//            LeftPlayerLabel[i].setDisable(true);
            p.leftSide.setTop(p.buttonLabel);
//            p.leftSide.getChildren().add(history);

        }

        //Center
        center.setMinSize(w * 2, h);

        String imagePath = "./images/gameplay_AC14.jpg";
        ImageView iv = null;
        try {
            iv = new ImageView(loadImage("./images/gameplay_AC14.jpg", w * 2, h));
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }

        center.getChildren().add(iv);

        //--------------------------------------------------------------------------------------------------
        //Right Side
        playerTurnAndPeiceHBox = new HBox();
        rightSide = new VBox();

        rightSide.setMinSize(w, h);
        rightSide.setMaxSize(w, h);
        rightSide.setPadding(new Insets(0, 10, 0, 10));
        rightSide.setSpacing(10);

//        playerTurnAndPeiceHBox.setBackground(new Background(new BackgroundFill(Color.rgb(h, h, h), CornerRadii.EMPTY, marginlessInsets)));
        playerTurnLabel.setFont(Font.font("Verdana", 20));

        rollLabel.setFont(Font.font("Verdana", 20));

        rolledTextVBox.setAlignment(Pos.CENTER_LEFT);
        rolledTextVBox.getChildren().add(rollLabel);

        playerTurnAndPeiceHBox.setPadding(new Insets(0, 10, 0, 10));
        playerTurnAndPeiceHBox.setAlignment(Pos.CENTER);
        playerTurnAndPeiceHBox.getChildren().add(playerTurnLabel);

        VBox topRightLabels = new VBox();
        topRightLabels.setAlignment(Pos.CENTER);
        topRightLabels.getChildren().add(playerTurnAndPeiceHBox);
        topRightLabels.getChildren().add(rolledTextVBox);
        rightSide.setBackground(new Background(new BackgroundFill(Color.rgb(221, 221, 221), new CornerRadii(0), new Insets(0, 0, 0, 0))));
        rightSide.getChildren().add(topRightLabels);

        die = new Button();
        die.setGraphic(dice[((int) ((Math.random() * 6) + 1))]);

        die.setOnMouseClicked(dieHandler);

        StackPane.setAlignment(die, Pos.CENTER);
        StackPane s = new StackPane();
        s.getChildren().add(die);
        rightSide.getChildren().add(s);

        VBox boxes = new VBox();

        HBox top = new HBox();
        top.setAlignment(Pos.CENTER);
        HBox bottom = new HBox();
        bottom.setAlignment(Pos.CENTER);

        Rectangle rect = new Rectangle(w / 4, w / 4);
        rect.setFill(Color.ORANGE);
        ssh = new SplashScreenHandlers(this, "top left nav box");
        rect.setOnMouseClicked(ssh);
        top.getChildren().add(rect);

        rect = new Rectangle(w / 4, w / 4);
        rect.setFill(Color.YELLOW);
        ssh = new SplashScreenHandlers(this, "top right   ht nav box");
        rect.setOnMouseClicked(ssh);
        top.getChildren().add(rect);

        rect = new Rectangle(w / 4, w / 4);
        rect.setFill(Color.GREENYELLOW);
        ssh = new SplashScreenHandlers(this, "bottom left nav box");
        rect.setOnMouseClicked(ssh);
        bottom.getChildren().add(rect);

        rect = new Rectangle(w / 4, w / 4);
        rect.setFill(Color.CYAN);
        ssh = new SplashScreenHandlers(this, "bottom right  nav box");
        rect.setOnMouseClicked(ssh);
        bottom.getChildren().add(rect);

        boxes.getChildren().add(top);
        boxes.getChildren().add(bottom);

        rightSide.getChildren().add(boxes);

        Button b = new Button();
        iv = null;

        try {
            iv = new ImageView(new Image(new File("./images/flight_plan.jpg").toURI().toURL().toExternalForm(), w - 20, h / 11, true, false));
            ssh = new SplashScreenHandlers(this, "flight_plan");
            iv.setOnMouseClicked(ssh);
            rightSide.getChildren().add(iv);

            iv = new ImageView(new Image(new File("./images/game_history.jpg").toURI().toURL().toExternalForm(), w - 20, h / 11, true, false));
            ssh = new SplashScreenHandlers(this, "history");
            iv.setOnMouseClicked(ssh);
            rightSide.getChildren().add(iv);

            iv = new ImageView(new Image(new File("./images/about_jte.jpg").toURI().toURL().toExternalForm(), w - 20, h / 11, true, false));
            ssh = new SplashScreenHandlers(this, "about_jte");
            iv.setOnMouseClicked(ssh);
            rightSide.getChildren().add(iv);

            iv = new ImageView(new Image(new File("./images/save.jpg").toURI().toURL().toExternalForm(), w - 20, h / 11, true, false));
            ssh = new SplashScreenHandlers(this, "save");
            iv.setOnMouseClicked(ssh);
            rightSide.getChildren().add(iv);

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }

        userNumMovesLeft = new Label("stuffffff");
        userNumMovesLeft.setMinSize(paneWidth / 4, paneHeight / 10);
        userNumMovesLeft.setMaxSize(paneWidth / 4, paneHeight / 10);
        userNumMovesLeft.setFont(new Font("Verdana", 20));
        userNumMovesLeft.setAlignment(Pos.CENTER_LEFT);
        userNumMovesLeft.setStyle("-fx-fill: WHITE");

        rightSide.getChildren().add(userNumMovesLeft);
        userNumMovesLeft.setVisible(false);

        dataManager.parseCSVFile(center, imagePath, w * 2, h);
        dataManager.parseXMLFile();

        Rectangle back = new Rectangle(paneWidth, paneHeight);
        back.setFill(Color.rgb(176, 176, 176));
        gamePane.getChildren().add(back);
//        center.getChildren().add(map);

        center.setOnMouseClicked(mch);
        gamePane.setLeft(players.get(currentPlayer).leftSide);
        gamePane.setCenter(center);
        gamePane.setRight(rightSide);
//        mainPane.getChildren().add(gamePane);
    }
    public Label userNumMovesLeft;

    public void processDiceClick(int n) {
        currentDieNumber = n;
        this.die.setGraphic(dice[n]);
//        if(n != 6)
        players.get(currentPlayer).hasRolled(true);
        players.get(getCurrentPlayer()).addNumMoves(n);
        updateTextOnRoll();
    }

    public void updateRegion(int x, int y) {
        int w = paneWidth / 2;
        int h = paneHeight;

        int wSec = w / 3; //A-C
        int hSec = h / 4; // 1-4 || 5-8

        int xtemp = (x / wSec);//*wSec;
        int ytemp = (y / hSec);//*hSec;

        region[0] = (char) xtemp;
//        region[1] = '0';
        region[1] = (char) ytemp;

        if (currentMapSection == 1) { // top left
            region[0] = (char) ((region[0] + 0) + 'A');
            region[1] = (char) ((region[1] + 0) + '0');
        } else if (currentMapSection == 2) { // top right
            region[0] = (char) ((region[0] + 0) + 'D');
            region[1] = (char) ((region[1] + 0) + '0');
        } else if (currentMapSection == 3) { // bottom left
            region[1] = (char) ((region[1] + 0) + '4');
        } else if (currentMapSection == 4) { // bottom right
            region[0] = (char) ((region[0] + 0) + 'D');
            region[1] = (char) ((region[1] + 0) + '4');
        }

    }

    public City getCityAt(Point p) {
        int x = p.x;
        int y = p.y;

        x = (x / threshold) * threshold;
        y = (y / threshold) * threshold;
        City c = dataManager.pointToCityMapping.get(new Point(x, y, currentMapSection));
        return c;
    }

    public boolean processMapClick(Point p) {

        //10px threshold
        int x = p.x;
        int y = p.y;

        x = (x / threshold) * threshold;
        y = (y / threshold) * threshold;
        City c = dataManager.pointToCityMapping.get(new Point(x, y, currentMapSection));
        if (c == null) {
            System.out.println("no city here");
        } else {
            updateRegion(p.x, p.y);
            System.out.println("city at this location is " + c.name);
            double w = paneWidth / 4;
            double h = paneHeight / 11;
            w = w / 5;
            h = h / 2.5;

            HBox visited = new HBox();
            visited.setSpacing(10);

            Rectangle rect = new Rectangle(w * 3, h * 2);
            Text text = new Text(c.name);
            text.setFont(Font.font("Verdana", 20));
            text.setFill(Color.GREEN);
            text.setBoundsType(TextBoundsType.VISUAL);
            StackPane stack = new StackPane();
            StackPane.setAlignment(rect, Pos.CENTER);
            StackPane.setAlignment(text, Pos.CENTER);
            stack.getChildren().addAll(rect, text);

            visited.getChildren().add(stack);

            rect = new Rectangle(w, h * 2);
            text = new Text("" + region[0]);
            Text text2 = new Text("" + region[1]);
            text.setFont(Font.font("Verdana", 30));
            text.setFill(Color.GREEN);
            text.setBoundsType(TextBoundsType.VISUAL);
            text2.setFont(Font.font("Verdana", 30));
            text2.setFill(Color.GREEN);
            text2.setBoundsType(TextBoundsType.VISUAL);
            stack = new StackPane();
            StackPane.setAlignment(text, Pos.TOP_LEFT);
            StackPane.setAlignment(text2, Pos.BOTTOM_RIGHT);
            stack.getChildren().addAll(rect, text, text2);
            Line line = new Line();
            line.setStartX(w);
            line.setStartY(0);
            line.setEndX(0);
            line.setEndY(h * 2);

            line.setFill(Color.RED);
            line.setStyle("-fx-border: 2px solid; -fx-border-color: white;");
            StackPane.setAlignment(line, Pos.CENTER);
            stack.getChildren().add(line);

            visited.getChildren().add(stack);

            history.getChildren().add(visited);

            return true;
        }
        return false;

    }

    public void runGameOpening() {

        loadCardsForPlayers();

    }

    /**
     * this method is how one says fuck you to all the thread issues
     */
    public void continueRunningGame() {
        System.out.println("in continueRunningGame");
        initPlayersToMaps();
        initPlayersWithNeighbors();
        loadPlayersToMap();

        d = new AI(this);

        Player p = players.get(currentPlayer);
        if (p.isComputer()) {
//            System.out.println("about to call p.getshortestPath with player:");
//            System.out.println("\t" + p);
            p.shortestPath = p.getShortestPaths(d);
            System.out.println("calling autorun from here: 954");
            Main.playerCanMove = true;
            autoRun();

        }

    }
    public static int readyForPathComputationSaftey = 0;

//    public void computeShortestPaths(){
//        for(int i= 0; i < numPlayers; i ++){
//            if(players.get(i).isComputer()){
//                System.out.println("finding shortest path for player " + (i + 1));
//                players.get(i).computeShortestPath();
//            }
//        }
//    }
    public void testLEEDS() {
        System.out.println("this function is no longer working correctly\n"
                + "now ending execution: testLEEDS()");
        System.exit(0);
//        System.out.println("LEEDS TESTING");
//        City c = dataManager.nameToTrueCityMapping.get("LEEDS");
//        System.out.println(c.name + " " + c.landNeighbors.size() + " " + c.seaNeighbors.size());
    }

    public void initPlayersWithNeighbors() {
        for (int i = 0; i < numPlayers; i++) {
            Player p = players.get(i);
            City home = p.currentCity;
//            System.out.print(p.name + ".currentCity: ");
//            System.out.println(p.currentCity.name);
            p.landCities = home.landNeighbors;
            p.seaCities = home.seaNeighbors;
        }
    }

    public void changeToNextPlayer() {

        if (PlayerTesting && players.get(getCurrentPlayer()).currentCity != null) {
            debugPrintln(PlayerTesting, players.get(getCurrentPlayer()).toString());
        }

        players.get(currentPlayer).previousCity = null;
        players.get(currentPlayer).hasRolled(false);

        Player old = players.get(getCurrentPlayer());

        ++currentPlayer;
//        System.out.println("currentPlayer: " + currentPlayer + "\tnumPlayers: " + numPlayers);
        currentPlayer = (currentPlayer) % (numPlayers);
        Player p = players.get(currentPlayer);
        gamePane.setLeft(p.leftSide);
        this.updateRightSidePlayerInfo();
//        if(theVoidHasBeenOpened){
////            loadPlayersToMap();
//        }
        if (p.quadrant != -1) {
            int quad = p.quadrant;
            String s = "";
            switch (quad) {
                case 1:
                    s = "top right";
                    break;
                case 2:
                    s = "top left";
                    break;
                case 3:
                    s = "bottom left";
                    break;
                case 4:
                    s = "bottom right";
                    break;
            }
            this.changeWorkspace(s);

        }

        rollLabel.setText("Please click on the\n die to roll");
        userNumMovesLeft.setText("Remaining Moves: " + p.getNumMoves());
        userNumMovesLeft.setStyle("-fx-fill: " + p.colorField.toUpperCase() + ";");
        userNumMovesLeft.setVisible(true);

//        if(getCurrentPlayer() == 0 && ! AnimationOnFinishedHandler.initCond){
//            readyForPathComputation = true;
//        }
//        if)
        System.out.println("if(" + players.get(getCurrentPlayer()).name + ".isComputer() && " + readyForPathComputation + ")");
        if (players.get(getCurrentPlayer()).isComputer() && readyForPathComputation) {
            if (p.isComputer()) {
//                d = new AI(this);

//                System.out.println("\tto continue uncomment line 1042 - UI");
//                p.shortestPath = p.getShortestPaths(d);
//                autoRun();
                refreshAlgoStuff();
                Main.playerCanMove = true;

            }
        }

        if (!AnimationOnFinishedHandler.initCond) {//readyForPathComputationSaftey == -1){
            readyForPathComputation = true;
            System.out.println("calling autoRun from here - 1048");
        }
    }

    public void refreshAlgoStuff() {
        System.out.println("refreshing gui");
        Player p = players.get(getCurrentPlayer());
//        d = new AI(this);

        System.out.println("\tto continue uncomment line 1042 - UI");

        p.shortestPath = p.getShortestPaths(d);
    }

    public boolean readyForPathComputation = false;

    public void autoRun() {
        Player p = players.get(getCurrentPlayer());

        if (!p.isComputer()) {
            return;
        }

        while (!p.hasRolled()) {
            dieHandler.loopHole();
        }
        System.out.println("\tcalling autoRun from JTEUI");
        autoRun(p.getNumMoves());
    }

    public void autoRun(int n) {
        System.out.println(n + " Auto Run Debug");

        Main.playerCanMove = false;

        Player p = players.get(getCurrentPlayer());
//        p.addNumMoves(10);
        ArrayList<City> path = p.shortestPath;

        System.out.println(n + " path.size: " + path.size());

        if (p.getNumMoves() > 0 && p.cards.size() > 0) {

            City curr = path.get(0);
            path.remove(0);

            if (path.isEmpty()) {// == 0){
                refreshAlgoStuff();
                System.out.println(n + " soemthing is wrong - empty: " + curr.name);
                path = p.shortestPath;
                path.remove(0);
            }
            City c = path.get(0);
            if (c.quadrent != p.quadrant) {
                changeWorkspace(c.quadrent);

            }
            System.out.println(curr.name + " --> " + path.get(0).name);

            center.getChildren().remove(p.ivGame);
            center.getChildren().add(p.ivGame);

            System.out.println(n + " map clicking by computer " + p.name);

            System.out.println(n + " " + p.name + " --> ##moves: " + p.getNumMoves());

            //--------------------------------------------
            mch.clickToMap(c.xReal, c.yReal, true);
            mch.clickToMap(c.xReal, c.yReal, true);

            //--------------------------------------------
            System.out.println(n + " " + p.name + " --> #moves: " + p.getNumMoves());

//            TranslateTransition tt = new TranslateTransition(Duration.millis(2000));
//            tt.setNode(p.ivGame);
//            tt.setByX(0);
//            tt.setByY(1);
//            tt.setDelay(Duration.millis(1001));
//            tt.setAutoReverse(true);
//            System.out.println(n + " before play : " + p.name);
//            tt.play();
//            
//            final Integer number = n;
//            tt.setOnFinished(new EventHandler<ActionEvent>(){
//                
//                @Override
//                public void handle(ActionEvent event) {
//                    System.out.println(number + " " + p.name + " event on finish handler for the fake movement");
//                    
//                    
//                    if (p.getNumMoves() == 0 && p.cards.size() != 0) {
//                        System.out.println(number + " changing to next player");
//                        changeToNextPlayer();
//                    } else if (p.cards.size() == 0) {
//                        System.out.println(p.name.toUpperCase() + " WON!!!!1");
//                    } else {
//                        System.out.println(number  + " else case 1123");
//                    }
//                    
//                    autoRun(number-1);
//                }
//            });
        }
//        outsideLoop:

//        privateP = null;
    }

    public void updateNumMoveText() {
        userNumMovesLeft.setText("Remaining Moves: " + players.get(getCurrentPlayer()).getNumMoves());
    }

    public void initPlayersToMaps() {
        System.out.println("initPlayersToMaps");
        for (int i = 0; i < numPlayers; i++) {
            Player p = players.get(i);
            if (p.cards.size() > 0) {
                try {
                    String cityName = p.cards.get(0).name;
                    //                City c = dataManager.nameToCityMapping.get(cityName);
                    City c = dataManager.nameToCityMapping.get(cityName);
                    //                System.out.println(p.name + "is @ city: " + c.name);
                    if (c == null) {
                        System.out.println("c is null");
                    }
                    if (p == null) {
                        System.out.println("p was null");
                    }
                    p.setPlayerAndImageX(c.xReal - p.playerImageWidth / 2);
                    p.setPlayerAndImageY(c.yReal - p.playerImageHeight);
                    p.flag.setPreserveRatio(true);
                    p.flag.setFitHeight(30);
                    //                p.flag.setX(c.x /- p.playerImageHeight-5 + 10);
                    p.flag.setX(c.x - 15);
                    p.flag.setY(c.y - 49);
    //                p.flag.setStyle("-fx-effect: dropshadow(three-pass-box, rgba(250,250,250,250.8), 10, 255, 255, 250);");
                    //                p.ivGame.setX(c.x - p.playerImageWidth/2);
                    //                p.ivGame.setY(c.y - p.playerImageHeight-5);
//                    System.out.println("setting " +p.name+ " currentCity to - " + c.name);
                    p.currentCity = c;
                    p.quadrant = c.quadrent;
                    p.flagQuadrant = c.quadrent;
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    System.out.println("loading crashed on Player " + (i + 1));
                    System.exit(0);
                }
            } else {
                System.out.println(p.name + " has an empty deck");
            }
        }
    }

//    private boolean theVoidHasBeenOpened = false;
//    
//    public void openTheVoid(){
//        theVoidHasBeenOpened = true;
//    }
    public static ArrayList<Line> linesToNeighbors = new ArrayList<Line>();

    public void loadPlayersToMap() {

        Player player = players.get(this.getCurrentPlayer());
        int quad = player.quadrant;
        String mapSection = "";

        if (quad == 1) {
            mapSection = "top left";
        } else if (quad == 2) {
            mapSection = "top right";
        } else if (quad == 3) {
            mapSection = "bottom left";
        } else if (quad == 4) {
            mapSection = "bottom right";
        } else {
            System.out.println("oops");
            System.exit(0);
        }
        this.changeWorkspace(mapSection);

        System.out.println("you did not remove the previous listeners");

        initLinesToNeighbors();

        player.ivGame.setOnMouseDragged(pdh);
        player.ivGame.setOnMouseReleased(pmrh);
        player.ivGame.setOnMousePressed(pmph);
    }

    public void initLinesToNeighbors() {

//        System.out.println("making lines of neighbors of " + players.get(getCurrentPlayer()).currentCity.name);
        linesToNeighbors.clear();

        Player player = players.get(getCurrentPlayer());
        ArrayList<City> landCities = player.getNeighboringCitiesLand();
        ArrayList<City> seaCities = player.getNeighboringCitiesSea();

        City root = player.currentCity;

        Line line = null;

        int strokeWidth = 3;

//        System.out.print("land lines: ");
        linesToNeighbors.clear();
        for (City c : landCities) {
            c = root.validateAndUpdateCity(c);

            line = new Line(c.xReal, c.yReal, root.xReal, root.yReal);
//            line.setFill(Color.RED);
//            line.setStyle("-fx-fill: #FF0000");
            line.setStroke(Color.RED);
            line.setStrokeWidth(strokeWidth);
            linesToNeighbors.add(line);

//            System.out.print("\n\t (" + root.x + ", " + root.y + ") --> (" + c.x + ", " + c.y + ")" );
        }
        for (City c : seaCities) {
            c = root.validateAndUpdateCity(c);
            line = new Line(c.xReal, c.yReal, root.xReal, root.yReal);
//            line.setFill(Color.RED);
            line.setStroke(Color.GREEN);
            line.setStrokeWidth(strokeWidth);
            linesToNeighbors.add(line);
//            System.out.print("\n\t (" + root.x + ", " + root.y + ") --> (" + c.x + ", " + c.y + ")" );
        }
//        System.out.println("");

    }

    public static int wasMoved = -1;

    public static int readyLoaded = 0;

    public void movePlayerToCity(Player p, City selectedCity, Point target, TranslateTransition tt, Animation animation, boolean isDrag) {
//        TranslateTransition tt = new TranslateTransition(new Duration(1001), player.ivGame);
        System.out.println("moving x,y by: (" + target.x + ", " + target.y + ")");
        tt.setByX(target.x);
        tt.setByY(target.y);
        
        tt.setAutoReverse(true);
        
        PauseTransition pt  = new PauseTransition(Duration.millis(Main.CardLoadSpeed));
        SequentialTransition st = new SequentialTransition(p.ivGame, pt, tt);
//        tt.setCycleCount(1);

//        tt.setOnFinished(new ClickMovementHandler(p, c, this, isDrag));
        City c = null;
        try {
            c = (City) selectedCity.clone();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
        st.setOnFinished(new ClickMovementHandler(p, c, this, isDrag));

//        animation.play();
        st.play();
//        tt.play();
    }

    public void loadCardsForPlayers() {

        ArrayList<String> cards = null;
        HashMap<String, Card> map = dataManager.cardsMap;
        Player p = players.get(currentPlayer);

        int nextCardColor = 0;

        nextCardColor = (players.get(this.getCurrentPlayer()).leftSide.getChildren().size() - 1 + this.getCurrentPlayer()) % 3;

        if (nextCardColor == 0) {
            cards = dataManager.redCards;
        } else if (nextCardColor == 1) {
            cards = dataManager.greenCards;
        } else if (nextCardColor == 2) {
            cards = dataManager.yellowCards;
        }

        map.get("TIRANE").used = true;

        int cardNum = (int) ((Math.random() * cards.size()));
        Card card = map.get(cards.get(cardNum).toString());

        while (card.used) {
            cardNum = (int) ((Math.random() * cards.size()));
            card = map.get(cards.get(cardNum));
        }

        card.used = true;
        p.cards.add(card);

        int startingY = 200;
        int startingX = (paneWidth / 4) + 100;
        double frac = card.front.getImage().getWidth() / ((paneWidth / 4) - players.get(currentPlayer).leftSide.getPadding().getRight() * 2);

        Button b = card.button;
        b.setGraphic(card.front);
        b.setLayoutX(startingX);
        b.setLayoutY(startingY);

//        System.out.println("about to start animation");
        final Duration SEC_1 = Duration.millis(Main.CardLoadSpeed);
        Animation animation = new Transition() {
            {
//                System.out.println("starting");
                setCycleDuration(Duration.millis(SEC_1.toMillis()));
            }

            protected void interpolate(double frac) {
            }
        };

        animation.play();
        animation.setOnFinished(aofh);

        TranslateTransition tt = new TranslateTransition(SEC_1);
//        tt.setToX(100f);
        tt.setFromX(startingX);
        tt.setFromY(startingY);
        tt.setToX(-(0.6 * startingX));//p.leftSide.getInsets().getLeft());
        tt.setToY(((paneHeight / 8) * (p.leftSide.getChildren().size() + 1)) - 20);
//        tt.setToY(p.buttonLabel.getHeight() + ( (paneHeight/8) * (p.leftSide.getChildren().size() - ) ));
        tt.setCycleCount(1);
        tt.setAutoReverse(false);

        ScaleTransition st = new ScaleTransition(SEC_1);
        double percent = ((paneWidth / 4) - (p.leftSide.getInsets().getLeft() * 2) - 20) / 450;
        st.setByX(0 - (1 - percent));
        st.setByY(0 - (1 - percent));
        st.setCycleCount(1);
        st.setAutoReverse(false);

        ParallelTransition pt = new ParallelTransition(b, st, tt);

        pt.play();

        players.get(currentPlayer).leftSide.getChildren().add(b);
    }

    public int getCurrentPlayer() {
//        lock.lock();
        int temp = currentPlayer;
//        lock.unlock();
        return temp;
    }

    public void setStage(Stage stage) {
        primaryStage = stage;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public BorderPane getMainPane() {
        return mainPane;
    }

    //From MiniGameFx
    public Image loadImage(String imageName) {

        Image img = null;
        try {
            img = new Image(new File(imageName).toURI().toURL().toExternalForm());
        } catch (Exception e) {
            e.printStackTrace();

        }
        return img;
    }

    public Image loadImage(String fileName, int width, int height) {

        File file = new File(fileName);
        if (file.exists()) {
            fileName = file.getAbsolutePath();
        }
        Image img = null;
        try {
            img = new Image(file.toURI().toURL().toExternalForm(), width, height, false, false, false);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
//            Image img = new Image(fileName, width, height, false, false, false);
        return img;

    }

    public Image loadImage(String fileName, int width, int height, boolean preservePixles, boolean smooth) {

        File file = new File(fileName);
        if (file.exists()) {
            fileName = file.getAbsolutePath();
        }
        Image img = null;
        try {
            img = new Image(file.toURI().toURL().toExternalForm(), width, height, preservePixles, smooth, false);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
//            Image img = new Image(fileName, width, height, false, false, false);
        return img;

    }

    public void changeWorkspace(int quad) {

        switch (quad) {
            case 1:
                this.changeWorkspace("top left");
                break;

            case 2:
                this.changeWorkspace("top right");
                break;

            case 3:
                this.changeWorkspace("bottom left");
                break;

            case 4:
                this.changeWorkspace("bottom right");
                break;
        }

    }

    public void changeWorkspace(String scene) {

        if (scene.toLowerCase().contains("start")) {
            mainPane.getChildren().clear();
            mainPane.getChildren().add(playerPane);
        } else if (scene.toLowerCase().contains("splash")) {
//            mainPane.getChildren().clear();
//            initSplashScreen();

            mainPane.getChildren().remove(aboutPane);

//            mainPane.setCenter(null);
//            mainPane.setRight(null);
//            mainPane.setLeft(null);
//            mainPane.getChildren().remove(mainPane.getChildren().size()-1);
//            mainPane.getChildren().add(playerPane);
        } else if (scene.toLowerCase().contains("about")) {
//            initAboutScreen();
            aboutPane = null;
            initAboutScreen();

        } else if (scene.toLowerCase().contains("load")) {

            loadFile();

        } else if (scene.toLowerCase().contains("die")) {
            System.out.println("need to do work for die click");

        } else if (scene.toLowerCase().contains("go")) {

            consolidatePlayerSetupScreen();
            mainPane.getChildren().clear();
            mainPane.getChildren().add(gamePane);
            System.out.println("initHistory being called: line 1625");
            initHistoryScreen();
            initFlightScreen();
            runGameOpening();
//            for(int i = 0; i < players.size(); i++){
//                BorderPane left = players.get(i).leftSide;
//                System.out.print(players.get(i).name + " - ");
//                for(int j = 0; j < left.getChildren().size(); j ++){
//                    System.out.print("\n\t" + left.getChildren().get(j));
//                }
//                System.out.println("");
//            }
//            loadCardsForPlayers();

        } else if (scene.toLowerCase().contains("win")) {

            final Stage dialogStage = new Stage();
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(getPrimaryStage());
            BorderPane modal = new BorderPane();
            Scene stfu = new Scene(modal, paneWidth - 200, paneHeight - 100);
            dialogStage.setScene(stfu);

            VBox contents = new VBox();
            contents.setAlignment(Pos.CENTER);
            contents.setSpacing(10);//(new Insets(10, 0, 0, 10));

            MiniGameImageView iv = new MiniGameImageView("./images/gameWin.png", (paneWidth - 200) / 2, 0, Color.rgb(51, 255, 102));
//            MiniGameImageView iv = new MiniGameImageView("./images/gameWin.png", (paneWidth-200)/2, 0, Color.rgb(75, 255, 83));

//            Label label = new Label("CONGRATS " + winner.name.toUpperCase() + "!!!! YOU WON!!!!!");
            Label label = new Label("CONGRATS " + winner.name.toUpperCase() + "!!!! YOU WON!!!!!");
            label.setFont(new Font("Verdana", 24));
            label.setStyle("-fx-font-fill: green;");
//            StackPane s = new StackPane();
//            s.setAlignment(Pos.CENTER);
//            s.getChildren().add(label);

            Button b = new Button();
            b.setGraphic(new Label("I feel pretty =D"));
            b.setDisable(true);
            // WHAT'S THE USER'S DECISION?
            b.setOnAction(new EventHandler<ActionEvent>() {
                @Override // Override the handle method
                public void handle(ActionEvent e) {
                    {
                        dialogStage.close();
                        changeWorkspace("history");
                    }
                }
            });

            contents.getChildren().add(iv);
            contents.getChildren().add(label);
            contents.getChildren().add(b);
            modal.setCenter(contents);
            ScaleTransition st = new ScaleTransition(Duration.millis(1000));
            st.setFromX(0.1);
            st.setFromY(0.1);
            st.setFromZ(0.1);
            st.setToX(1);
            st.setToY(1);
            st.setToZ(1);
//            st.play();

            RotateTransition rt = new RotateTransition(Duration.millis(1000));
            rt.setByAngle(360 * 5);

//            TranslateTransition timeKiller = new TranslateTransition(Duration.millis(1000));
            ParallelTransition pt = new ParallelTransition(contents, st);
//            SequentialTransition stt = new SequentialTransition(contents, pt, timeKiller);
            pt.play();

            pt.setOnFinished(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
//                    System.exit(0);
                    b.setDisable(false);
                }
            });

            dialogStage.show();

        } else if (scene.toLowerCase().contains("top left")) {
            mainPane.getChildren().remove(aboutPane);
            gamePane.getChildren().remove(historyPane);
            gamePane.setRight(rightSide);

            currentMapSection = 1;

            if (wasMoved != -1) {
                for (int i = 0; i < center.getChildren().size(); i++) {
                    if (center.getChildren().get(i) instanceof ImageView) {
                        if (((ImageView) center.getChildren().get(i)) != players.get(currentPlayer).ivGame) {
                            center.getChildren().remove(i);
                        }
                    }
                }
                pmrh.clearLines();
            } else {
                center.getChildren().clear();
//                System.out.println("center cleared");
            }

            center.getChildren().add(0, dataManager.mapQuadrants[currentMapSection]);
            loadPlayersToMapChangeRequest(1);

        } else if (scene.toLowerCase().contains("top right")) {
            mainPane.getChildren().remove(aboutPane);
            gamePane.getChildren().remove(historyPane);

            currentMapSection = 2;
            if (wasMoved != -1) {

                for (int i = 0; i < center.getChildren().size(); i++) {
                    if (center.getChildren().get(i) instanceof ImageView) {
                        if (((ImageView) center.getChildren().get(i)) != players.get(currentPlayer).ivGame) {
                            center.getChildren().remove(i);
                        }
                    }
                }
                pmrh.clearLines();
            } else {
                center.getChildren().clear();
//                System.out.println("center cleared");
            }
            center.getChildren().add(0, dataManager.mapQuadrants[currentMapSection]);
            loadPlayersToMapChangeRequest(2);
//            if(wasMoved != -1){
//                center.getChildren().add(players.get(currentPlayer).ivGame);
//                System.out.println("reloading player");
//            }

        } else if (scene.toLowerCase().contains("bottom left")) {
            mainPane.getChildren().remove(aboutPane);
            gamePane.getChildren().remove(historyPane);
            currentMapSection = 3;
            if (wasMoved != -1) {
                for (int i = 0; i < center.getChildren().size(); i++) {
                    if (center.getChildren().get(i) instanceof ImageView) {
                        if (((ImageView) center.getChildren().get(i)) != players.get(currentPlayer).ivGame) {
                            center.getChildren().remove(i);
                        }
                    }
                    pmrh.clearLines();
                }
//                wasMoved = -1;
            } else {
                center.getChildren().clear();
//                System.out.println("center cleared");
            }
            center.getChildren().add(0, dataManager.mapQuadrants[currentMapSection]);
            loadPlayersToMapChangeRequest(3);
//            if(wasMoved != -1){
//                center.getChildren().add(players.get(currentPlayer).ivGame);
//                System.out.println("reloading player");
//            }

        } else if (scene.toLowerCase().contains("bottom right")) {
            mainPane.getChildren().remove(aboutPane);
            gamePane.getChildren().remove(historyPane);
            currentMapSection = 4;
            if (wasMoved != -1) {
                for (int i = 0; i < center.getChildren().size(); i++) {
                    if (center.getChildren().get(i) instanceof ImageView) {
                        if (((ImageView) center.getChildren().get(i)) != players.get(currentPlayer).ivGame) {
                            center.getChildren().remove(i);
                        }
                    }
                    pmrh.clearLines();

                }
//                wasMoved = -1;
            } else {
                center.getChildren().clear();
            }
            center.getChildren().add(0, dataManager.mapQuadrants[currentMapSection]);
            loadPlayersToMapChangeRequest(4);
//            if(wasMoved != -1){
////                center.getChildren().add(players.get(currentPlayer).ivGame);
//                System.out.println("reloading player");
//            }

        } else if (scene.toLowerCase().contains("flight_plan")) {
            flightPlanClicked();

        } else if (scene.toLowerCase().contains("history")) {
//            changeWorkspace("top left");

            for (int i = 0; i < numPlayers; i++) {
                ArrayList<String> list = players.get(i).history;
                String past = "";
                int j = 0;
                for (; j < list.size(); j++) {
                    past += list.get(j) + "\n";
                }
                historyBoxes.get(i).setText(past);
                if (j == 0) {
                    historyBoxes.get(i).setText("Sorry no History yet");
                }
            }

            if (!gamePane.getChildren().contains(historyPane)) {
                gamePane.getChildren().add(historyPane);
            } else {
                gamePane.getChildren().remove(historyPane);
            }

        } else if (scene.toLowerCase().contains("about_jte")) {
            mainPane.getChildren().remove(historyPane);
            changeWorkspace("about");

        } else if (scene.toLowerCase().contains("save")) {
            if (winner != null) {
                this.saveFile();
                System.exit(0);

            } else {
                final Stage dialogStage = new Stage();
                dialogStage.initModality(Modality.WINDOW_MODAL);
                dialogStage.initOwner(getPrimaryStage());
                BorderPane modal = new BorderPane();
                Scene stfu = new Scene(modal, 300, 200);
                dialogStage.setScene(stfu);

                Label l = new Label("Saving is not an option when the game as been saved! Sorry =*(");

                l.setFont(new Font("Verdana", 20));
                l.setAlignment(Pos.CENTER);
                l.setTextAlignment(TextAlignment.CENTER);

                Pane pane = new Pane();
                pane.setMinSize(300, 200);

                pane.getChildren().add(l);
                modal.getChildren().add(pane);

                TranslateTransition tt = new TranslateTransition(Duration.millis(1000));

                tt.play();
                tt.setOnFinished(new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent event) {
                        dialogStage.close();
                    }
                });

                dialogStage.show();
            }
        } else if (scene.toLowerCase().contains("quit")) {
            System.out.println("program exiting");
            System.exit(0);

        } else {
            System.out.println("something is messed up it should not be hitting here\n"
                    + "you need to add implementation to the changeWorkspace method in the UI class");
        }
//        System.out.println("wasMoved: " + wasMoved);
    }

    public void loadPlayersToMapChangeRequest(int quad) {
//        System.out.println("loadPlayersToMapChangeRequest( " + quad + ")");
        for (int i = 0; i < numPlayers; i++) {
            Player p = players.get(i);
            if (p.quadrant == quad) {
                try {
                    center.getChildren().add(p.ivGame);
                } catch (IllegalArgumentException e) {
                    System.out.println("its ok... dont be angry fx");
                } catch (Exception e) {
                    System.out.println("caught something with: " + p.ivGame);
                    e.printStackTrace();
                    System.exit(0);
                }
            }

            if (p.flagQuadrant == quad) {
                try {
                    center.getChildren().add(p.flag);
                } catch (IllegalArgumentException e) {
                    System.out.println("its ok... dont be angry fx");
                } catch (Exception e) {
                    System.out.println("caught something with: " + p.ivGame);
                    e.printStackTrace();
                    System.exit(0);
                }

            }
        }
    }

    public void loadFailureToRollDialogBox() {

        final Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(getPrimaryStage());
        BorderPane exitPane = new BorderPane();
        HBox optionPane = new HBox();
        Button yesButton = new Button("Mmkay?");
//                Button noButton = new Button("");
        optionPane.setSpacing(10.0);
        optionPane.getChildren().add(yesButton);//, noButton);
        Label label = new Label("You must roll before you can move!");
        StackPane s = new StackPane();
        s.setAlignment(Pos.CENTER);
        s.getChildren().add(label);

        exitPane.setTop(s);

//                s = new StackPane();
//                s.setMinWidth(399);
//                s.setMaxWidth(400);
//                s.setAlignment(Pos.CENTER);
        optionPane.setAlignment(Pos.CENTER);
//                s.getChildren().add(optionPane);

        exitPane.setCenter(optionPane);

        Scene scene = new Scene(exitPane, 300, 100);
        dialogStage.setScene(scene);
        dialogStage.show();
        // WHAT'S THE USER'S DECISION?
        yesButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override // Override the handle method
            public void handle(ActionEvent e) {
                {
                    dialogStage.close();
                    dieHandler.loopHole();
                }
            }
        });
    }

    public void loadFailureToRollDialogBox(String message) {

        final Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(getPrimaryStage());
        BorderPane exitPane = new BorderPane();
        HBox optionPane = new HBox();
        Button yesButton = new Button("OK?");
//                Button noButton = new Button("");
        optionPane.setSpacing(10.0);
        optionPane.getChildren().add(yesButton);//, noButton);
        Label label = new Label(message);
        StackPane s = new StackPane();
        s.setAlignment(Pos.CENTER);
        s.getChildren().add(label);

        exitPane.setTop(s);

//                s = new StackPane();
//                s.setMinWidth(399);
//                s.setMaxWidth(400);
//                s.setAlignment(Pos.CENTER);
        optionPane.setAlignment(Pos.CENTER);
//                s.getChildren().add(optionPane);

        exitPane.setCenter(optionPane);

        Scene scene = new Scene(exitPane, 300, 100);
        dialogStage.setScene(scene);
        dialogStage.show();
        // WHAT'S THE USER'S DECISION?
        yesButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override // Override the handle method
            public void handle(ActionEvent e) {
                {
                    dialogStage.close();
//                                dieHandler.loopHole();
                }
            }
        });
    }

}
