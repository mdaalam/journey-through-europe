package JourneyUI;

import java.io.File;
import java.net.MalformedURLException;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class MiniGameImageView extends ImageView implements Cloneable{
    
    private String path;
    private int width;
    private int height;
    private Color colorKey = null;
    private int loadMethod;

    private MiniGameImageView(){
        path = null;
        width = -1;
        height = -1;
    }

    public MiniGameImageView(String path){
        super(loadImage(path));
        this.path = path;
        this.loadMethod = 1;
    }

    public MiniGameImageView(WritableImage wi){
        super(wi);
//        this.path = path;
//        this.loadMethod = 1;
    }
    /**
     * Allocates a new ImageView object using the given 
     * path, width and height.
     * 
     * @param path relative or absolute path
     * to image you wish to load
     * @param width - in pixels
     * @param height - in pixels
     */
    public MiniGameImageView(String path, int width, int height) {
        super(loadImage(path, width, height));
        this.width = width;
        this.height = height;
        this.path = path;
        this.loadMethod = 2;
    }

    /**
     * Allocates a new ImageView object using the given 
     * path, width and height.
     * 
     * @param path relative or absolute path
     * to image you wish to load.
     * @param width - in pixels.
     * @param height - in pixels.
     * @param background - boolean, value is does not 
     * matter, boolean just required to be passed.
     * 
     */
    public MiniGameImageView(String path, int width, int height, boolean background) {
        super(loadImageAsBackground(path, width, height));
        this.width = width;
        this.height = height;
        this.path = path;
        this.loadMethod = 3;
    }

    /**
     * Allocates a new ImageView object using the given 
     * path, width, height, and colorKey.
     * 
     * Loads an image using the fileName as the full path,
     * returning the constructed and completely loaded Image. Note
     * that all pixels with the colorKey value will be made
     * transparent by setting their alpha values to 0.
     * 
     * @param path relative or absolute path
     * to image you wish to load.
     * @param width - in pixels.
     * @param height - in pixels.
     * @param colorKey - color key for color you wish to make 
     * transparent
     */
    public MiniGameImageView(String path, int width, int height, Color colorKey) {
        super(loadImageWithColorKey(path, width, height, colorKey));
        this.path = path;
        this.colorKey = colorKey;
        this.width = width;
        this.height = height;
        this.path = path;
        this.loadMethod = 4;
    }

    /**
     * Construct an Image with content loaded from the specified 
     * input stream.
     * 
     * @param fileName can be relative or absolute path
     * @return Image object at the location passed
     */
    public static Image loadImage(String fileName) {
        File file = new File(fileName);
        if (file.exists()) {
            fileName = file.getAbsolutePath();
        }
        Image img = null;
        try {
            img = new Image(file.toURI().toURL().toExternalForm());
        } catch (Exception e) {
            System.out.println("image could not be loaded ");
            e.printStackTrace();
        }
        return img;

    }


    /**
     * Loads image with custom width and height
     * uses default non-image-smoothing algorithm
     * @param fileName - relative or absolute
     * @param width - in pixels
     * @param height - in pixels
     * @return 
     */
    public static Image loadImage(String fileName, int width, int height){

        File file = new File(fileName);
        if(file.exists())
            fileName = file.getAbsolutePath();
        Image img = null;
        try{
            img= new Image(file.toURI().toURL().toExternalForm(), width, height, false, false, false);
        }catch(MalformedURLException e){
            e.printStackTrace();
        }
//            Image img = new Image(fileName, width, height, false, false, false);
        return img;

    }

    /**
     * Load image with custom width and height
     * with intent of being used as background
     * uses default non-image-smoothing-algorithm
     * @param fileName - relative or absolute
     * @param width - in pixels
     * @param height - in pixels
     * @return 
     */
    public static Image loadImageAsBackground(String fileName, int width, int height){
        Image img = new Image(fileName, width, height, false, false, true);
        return img;
    }

    /**
     * Loads an image using the fileName as the full path,
     * returning the constructed and completely loaded Image. Note
     * that all pixels with the colorKey value will be made
     * transparent by setting their alpha values to 0.
     * 
     * @param fileName full path and name of the location of
     * the image file to be loaded.
     * @param width - in pixels
     * @param height - in pixels
     * @param colorKey - Color you wish to ignore and load
     * as transparent pixels
     * 
     * 
     * @return the loaded Image, with all data fully loaded and
     * with colorKey pixels transparent.
     */
    public static Image loadImageWithColorKey(String fileName, int width, int height, Color colorKey)
    {
        Image img = null;
        ImageView iv = new ImageView();
        File file = new File(fileName); 
        try{
            if(width == 0 && height == 0){
                img= new Image(file.toURI().toURL().toExternalForm());
            }else if (width != 0 && height == 0){
                img= new Image(file.toURI().toURL().toExternalForm(), width, height, true, false);
            }
            else{
                img= new Image(file.toURI().toURL().toExternalForm(), width, height, false, false);
            }
        }catch(MalformedURLException e){
            e.printStackTrace();
            System.exit(0);
        }

        WritableImage wi = new WritableImage((int)img.getWidth(), (int)img.getHeight());
        PixelWriter px = wi.getPixelWriter();
        iv.setImage(img);

        PixelReader pr = iv.getImage().getPixelReader();

        for(int x = 0; x < img.getWidth(); x ++){
            for(int y = 0; y < img.getHeight(); y ++){
                Color c = pr.getColor(x, y);
                Color temp;
                if(c.getBlue() == colorKey.getBlue() && c.getRed() == colorKey.getRed() && c.getGreen() == colorKey.getGreen()){
                    temp = new Color(c.getRed(), c.getGreen(), c.getBlue(), 0);
                }else{
                    temp = new Color(c.getRed(), c.getGreen(), c.getBlue(), 1);
                }
                px.setColor(x, y, temp);
            }
        }
        return wi;
    }
    
    /**
     * Custom deep clone method
     * 
     * @return deep-cloned MiniGameImageView 
     * object (this)
     */
    public MiniGameImageView clone(){
        MiniGameImageView temp = null;
        
        switch(this.loadMethod){
            case 1: 
                temp = new MiniGameImageView(path);
                break;
            case 2: 
                temp = new MiniGameImageView(path, width, height);
                break;
            case 3: 
                temp = new MiniGameImageView(path, width, height, true);
                break;
            case 4: 
                temp = new MiniGameImageView(path, width, height, colorKey);
                break;
        }
        return temp;
    }
}
