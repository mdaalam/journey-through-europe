package JourneyUI;

import static JourneyUI.JTEUI.threshold;
import Objects.Card;
import Objects.City;
import Objects.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Scanner;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class JTEDataManager {
    public static JTEUI ui;

    public static HashMap<String, String> cityInfo;
    public ArrayList<String> greenCards;
    public ArrayList<String> redCards;
    public ArrayList<String> yellowCards;
    public ArrayList<City> allCities;
    public HashMap<String, Card> cardsMap;
    
    public HashMap<Point, City> pointToCityMapping;
    public HashMap<String, City> nameToCityMapping;
    public ArrayList<City> airportList;
      
    public ImageView [] mapQuadrants;
    public ImageView [] originalMaps;
    
    public static int deathMode = 1;
    public JTEDataManager(JTEUI ui){
        if(deathMode != 1){
            try{
                throw new ConcurrentModificationException("You're an idiot");
            }catch(Exception e){
                e.printStackTrace();
                System.exit(0);
            }
            deathMode ++;
        }
        this.ui = ui;
        greenCards = new ArrayList<String>();
        yellowCards = new ArrayList<String>();
        redCards = new ArrayList<String>();
        allCities = new ArrayList<City>();
        cardsMap = new HashMap<String, Card>();
        pointToCityMapping = new HashMap<Point, City>();
        nameToCityMapping = new HashMap<String, City>();
        airportList = new ArrayList<City>();
        cityInfo = new HashMap<String, String>();
        
        mapQuadrants = new ImageView[5];
        originalMaps = new ImageView[5];
        try{

            originalMaps[1] = new ImageView(new File("./images/gameplay_AC14.jpg").toURI().toURL().toExternalForm());
            originalMaps[2] = new ImageView(new File("./images/gameplay_DF14.jpg").toURI().toURL().toExternalForm());
            originalMaps[3] = new ImageView(new File("./images/gameplay_AC58.jpg").toURI().toURL().toExternalForm());
            originalMaps[4] = new ImageView(new File("./images/gameplay_DF58.jpg").toURI().toURL().toExternalForm());
            
            mapQuadrants[1] = new ImageView(ui.loadImage("./images/gameplay_AC14.jpg", ui.paneWidth/2, ui.paneHeight));
            mapQuadrants[2] = new ImageView(ui.loadImage("./images/gameplay_DF14.jpg", ui.paneWidth/2, ui.paneHeight));
            mapQuadrants[3] = new ImageView(ui.loadImage("./images/gameplay_AC58.jpg", ui.paneWidth/2, ui.paneHeight));
            mapQuadrants[4] = new ImageView(ui.loadImage("./images/gameplay_DF58.jpg", ui.paneWidth/2, ui.paneHeight));  
        }catch(Exception e){
            e.printStackTrace();
        }
        
        parseCityInfoFile();
        if(JTEUI.isLoading){
            parseXMLFile();
            parseCityInfoFile();
            importCards();
            importAirportInformation();
        }
    }
    
    public void parseCityInfoFile(){
        Scanner sc = null;
        File file = new File("./misc/towns.txt");
        try{
            sc = new Scanner(file);
        }catch(Exception e){
            e.printStackTrace();
        }
        
        while(sc.hasNextLine()){
            String name = sc.nextLine().trim();
            String message = sc.nextLine().trim();
            cityInfo.put(name, message);
        }
        
    }
    
    public void importAirportInformation(){
        File file = new File("./misc/flights.txt");
        Scanner sc = null;
        
        try{
            sc = new Scanner(file);
        }catch(FileNotFoundException e){
            System.out.println(file.getAbsoluteFile());
            e.printStackTrace();
        }
        
        while(sc.hasNextLine()){
            Scanner line = new Scanner(sc.nextLine());
            
            while(line.hasNext()){
                String name = line.next();
                int quad = line.nextInt();
                if(quad == 4){
                    quad = 3;
                }else if(quad ==3 ){
                    quad =4;
                }
                int x = line.nextInt()/2;
                int y = line.nextInt()/2;
                City old = nameToCityMapping.get(name);
                old.turnIntoAirport();
                old.airPointCoords = new Point((20*(x/20)) + 10, (20*(y/20)) + 10);
                airportList.add(old);
            }
        } 
    }
    
    public void importCards(){
        ArrayList<String> list;
        String filePath = "";
        for(int i = 0; i < 3; i ++){
            if(i == 0){
                list = greenCards;
                filePath = "./misc/greenCards.txt";
            }else if(i == 2){
                list = redCards;
                filePath = "./misc/redCards.txt";
            }else{
                list = yellowCards;
                filePath = "./misc/yellowCards.txt";
            }
            
            File file = null;
            Scanner sc = null;
            
            try{
                file = new File(filePath);
                sc = new Scanner(file);
            }catch(Exception e){
                System.out.println(file.getAbsoluteFile());
                e.printStackTrace();
                System.exit(0);
            }
            
            ImageView iv = null;
            while(sc.hasNextLine()){
                String line = "./images" + sc.nextLine().substring(1);
                String place = line.substring(line.lastIndexOf("/")+1, line.length()-4).toUpperCase();
                
                Card temp = cardsMap.get(place);
                
                try{
                    iv = new ImageView(new Image(new File(line).toURI().toURL().toExternalForm(), 450, 640, false, false));
                }catch(Exception e){
                    e.printStackTrace();
                    System.exit(0);
                }
                
                if(line.contains("_I.jpg")){ // is the back of an image
                    line = line.substring(0, line.length()-6) + ".jpg";
                    if(temp == null){
                        place = place.substring(0, place.length()-2);
                        cardsMap.put(place, new Card(line, null, iv));
                    }else{
                        temp.back = iv;
                    }
                }else{
                    if(temp == null){
                        cardsMap.put(place, new Card(line, iv, null));
                    }else{
                        temp.front = iv;
                    }
                }
                
                list.add(place);
            } 
        }
    }
    
    public void parseCSVFile(Pane center, String imagePath, int w, int h) {
        Image img = null;
        try {
            img = new Image(new File(imagePath).toURI().toURL().toExternalForm());
        } catch (Exception e) {
            System.out.println(new File(imagePath).getAbsoluteFile());
            e.printStackTrace();
            System.exit(0);
        }

        File file = null;
        Scanner sc = null;

        try {
            file = new File("./misc/cities.txt");
            sc = new Scanner(file);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }

        sc.nextLine();

        while (sc.hasNextLine()) {

            Scanner s = new Scanner(sc.nextLine());
            s.useDelimiter("\t");
            while (s.hasNext()) {

                String name = s.next();
                String color = s.next();
                int quadrant = Integer.parseInt(s.next());
                int x = Integer.parseInt(s.next());
                int y = Integer.parseInt(s.next());
                
                x = (int) (x * ((0.0 + w) / originalMaps[quadrant].getImage().getWidth()));
                y = (int) (y * ((0.0 + h) / originalMaps[quadrant].getImage().getHeight()));
                int realX = x;
                int realY = y;
                x = (x / threshold) * threshold;
                y = (y / threshold) * threshold;

                City c = new City(name, color, quadrant, x, y, realX, realY);
                allCities.add(c);
                pointToCityMapping.put(new Point(x, y, quadrant), c);
                nameToCityMapping.put(name, c);
            }
        }
    }
    
    public boolean isLEEDS = false;
    
    public void parseXMLFile() {
        File file = new File("./misc/citiesxml.txt");

        Scanner sc = null;
        try {
            sc = new Scanner(file);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String city = "";
        String land = "";
        String sea = "";
        City c = null;
        String line = "";
        while (sc.hasNext()) {
            line = sc.nextLine().trim();
            if ("<city>".equals(line)) {
                city = "";
                land = "";
                sea = "";
            } else if (line.indexOf("<name>") == 0) {
                String cityName = cutTags(line);
                c = nameToCityMapping.get(cityName);
//                if(c.name.equals("LEEDS")){
//                    isLEEDS = true;
//                }
                if( c == null){
                    System.out.println("You really messed up this time- " + cityName);
                    System.exit(0);
                }
            } else if (line.indexOf("<land>") == 0) {
//                land += 
                parseLandTags(sc, line, c);
            } else if (line.indexOf("<sea>") == 0) {
//                sea += 
                parseSeaTags(sc, line, c);
            }

//            System.out.println(city + " --> \n\t" + land + "\n\t" + sea);

        }
    }

    public void parseLandTags(Scanner sc, String line, City c) {

        String temp = "";
        line = sc.nextLine().trim();

        while (!line.equals("</land>")) {
            if (line.indexOf("<city>") == 0) {
                if (!"<city>".equals(line)) {
                    temp = cutTags(line);
                    City neighbor = null;
//                    try{
                        neighbor = nameToCityMapping.get(temp);//.clone();
//                    }catch(CloneNotSupportedException e){
//                        e.printStackTrace();  
//                    }
                        if(neighbor == null){
                            System.out.println("failur imminent on city: " + temp);
                        }
                    if( ! c.landNeighbors.contains(neighbor)){
                        c.landNeighbors.add(neighbor);
                    }
                    /**
                     * this will fill the map with reverse links to ensure 
                     * that it is complete
                     */
                    if( ! neighbor.landNeighbors.contains(c)){
                        neighbor.landNeighbors.add(c);
                    }
                    
                    if(isLEEDS){
                        System.out.println("found land neighboor - " + neighbor.name);
                    }
                    if(neighbor == null){
                        System.out.println("one of the land neighbors is null -- " + temp);
                        System.exit(0);
                    }
                } else {
                    System.out.println("you shouldnt assume things - land");
                    System.exit(0);
                }
            }
            line = sc.nextLine().trim();
        }
//        return temp;
    }

    public void parseSeaTags(Scanner sc, String line, City c) {
        String temp = "";
        line = sc.nextLine().trim();

        while (!line.equals("</sea>")) {
            if (line.indexOf("<city>") == 0) {
                if (!"<city>".equals(line)) {
                    temp = cutTags(line);
                    City neighbor = null;
//                    
                    neighbor = nameToCityMapping.get(temp);
                    if( ! c.seaNeighbors.contains(neighbor)){
                        c.seaNeighbors.add(neighbor);
                    }
                    /**
                     * this will fill the map with reverse links to ensure 
                     * that it is complete
                     */
                    if( ! neighbor.seaNeighbors.contains(c)){
                        neighbor.seaNeighbors.add(c);
                    }
                    if(neighbor == null){
                        System.out.println("one of the sea neighbors is null -- " + temp);
                        System.exit(0);
                    }
                } else {
                    System.out.println("you shouldnt assume things - sea");
                    System.exit(0);
                }
            }
            line = sc.nextLine().trim();
        }
    }

    public static String cutTags(String x) {
        return x.substring(x.indexOf(">") + 1, x.lastIndexOf("<"));
    }

}
